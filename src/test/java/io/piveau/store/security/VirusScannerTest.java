package io.piveau.store.security;

import io.vertx.core.Vertx;
import io.vertx.ext.web.FileUpload;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import static org.mockito.Mockito.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.junit.jupiter.Testcontainers;
import xyz.capybara.clamav.commands.scan.result.ScanResult;

import java.io.IOException;
import java.nio.file.*;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static xyz.capybara.clamav.commands.scan.result.ScanResult.*;




@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Testcontainers(disabledWithoutDocker = true)
public class VirusScannerTest {

    @TempDir
    public Path tempDir;

    private VirusScanner scanner;

    private final GenericContainer container = new GenericContainer("clamav/clamav")
            .withExposedPorts(3310)
            .waitingFor(new LogMessageWaitStrategy()
                    .withRegEx(".*socket found, clamd started.*")
                    .withStartupTimeout(Duration.of(60, ChronoUnit.SECONDS)));


    @BeforeAll
    public void setup() {
        container.start();
        scanner = new VirusScanner(container.getHost(), container.getMappedPort(3310));
    }
    @AfterAll
    public void tearDown() {
        container.stop();
    }

    @Test
    @DisplayName("test a clean file")
    void testScanCleanFile(Vertx vertx, VertxTestContext testContext) throws IOException {
        // Create a clean file
        String cleanFilePath = System.getProperty("java.io.tmpdir") + "/clean_"+ UUID.randomUUID() +".txt";


        Files.writeString(Path.of(cleanFilePath), "This is a clean file.", StandardOpenOption.CREATE_NEW);

        // Create a FileUpload to send the file to the scanner
        FileUpload cleanFileUpload = mock(FileUpload.class);
        when(cleanFileUpload.uploadedFileName()).thenReturn(cleanFilePath);
        when(cleanFileUpload.contentType()).thenReturn("text/plain");

        // Call the scanner's scan method and assert the result
        ScanResult sr = scanner.scanFile(cleanFileUpload);


            testContext.verify(() -> {
                Assertions.assertFalse(sr instanceof ScanResult.VirusFound );
                testContext.completeNow();
            });

    }

    @Test
    @DisplayName("test an infected file")
    void testScanInfectedFile(Vertx vertx, VertxTestContext testContext) throws IOException {

        Path eicarFilePath = tempDir.resolve("eicar.txt");
        Files.writeString(eicarFilePath, "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*", StandardOpenOption.CREATE_NEW);


        // Mock the FileUpload object
        FileUpload infectedFileUpload = mock(FileUpload.class);
        when(infectedFileUpload.uploadedFileName()).thenReturn(eicarFilePath.toString());
        when(infectedFileUpload.contentType()).thenReturn("text/plain");

        // Call the scanner's scanFile method and assert the result


        ScanResult sr = scanner.scanFile(infectedFileUpload);


        testContext.verify(() -> {
            Assertions.assertInstanceOf(VirusFound.class, sr);
            Files.deleteIfExists(eicarFilePath); // Cleanup the temporary file
            testContext.completeNow();
        });
    }

}