package io.piveau.store;

import com.google.common.base.Strings;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.JWTOptions;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.ext.web.multipart.MultipartForm;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Arrays;
import java.util.UUID;

@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Testcontainers(disabledWithoutDocker = true)
public class StoreApiTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private WebClient client;

    private String token;
    JsonObject config;


    private final MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:4.4.11"));

    @BeforeAll
    void setup(Vertx vertx, VertxTestContext testContext) {
        mongoDBContainer.start();

        JWTAuthOptions jwtAuthOptions = new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                        .setAlgorithm("HS256")
                        .setBuffer("secret"));

        JWTAuth jwtAuth = JWTAuth.create(vertx, jwtAuthOptions);

        JsonObject authorization = new JsonObject().put("permissions", new JsonArray().add(
                new JsonObject()
                        .put("scopes", new JsonArray().add("dataset:update").add("dataset:delete").add("dataset:create"))
                        .put("rsname", "catalog-1")
        ));

        token = jwtAuth.generateToken(new JsonObject()
                .put("authorization", authorization)
                .put("realm_access", new JsonObject().put("roles", new JsonArray().add("provider"))), new JWTOptions()
                .setNoTimestamp(true)
                .setSubject("test"));
        log.info(token);
        config = new JsonObject()
                .put(Constants.ENV_STORAGE_BACKEND, "FILE")
//                .put(Constants.ENV_STORAGE_BACKEND, "S3")
                .put(Constants.ENV_S3_ENDPOINT, "http://127.0.0.1:9000")
                .put(Constants.ENV_S3_ACCESSKEY, "minioadmin")
                .put(Constants.ENV_S3_SECRET, "minioadmin")
                .put(Constants.ENV_PIVEAU_STORE_AUTH_CONFIG, new JsonObject().put("customSecret", "secret"))
                .put(Constants.ENV_MONGO_DB_URI, mongoDBContainer.getConnectionString())
                .put(Constants.ENV_MONGO_DB_NAME, "piveau")
                .put(Constants.ENV_ALLOWED_MIME_TYPES, new JsonArray(Arrays.asList("image/", "text/plain", "application/vnd.", "text/csv")));
        vertx.deployVerticle(io.piveau.store.MainVerticle.class, new DeploymentOptions().setConfig(config), ar -> {
            if (ar.succeeded()) {
                client = WebClient.create(vertx);
                testContext.completeNow();
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    @AfterAll
    void stop(Vertx vertx, VertxTestContext testContext) {
        vertx.fileSystem().exists(Constants.STORAGE_DEFAULT_LOCATION)
                .compose(exists -> {
                    if (exists) {
                        return vertx.fileSystem().deleteRecursive(Constants.STORAGE_DEFAULT_LOCATION, true);
                    } else {
                        return Future.succeededFuture();
                    }
                })
                .onSuccess(v -> testContext.completeNow())
                .onFailure(testContext::failNow);
    }

    private String location = "";

    @Test
    @DisplayName("Upload image file")
    @Order(1)
    void uploadPngFile(Vertx vertx, VertxTestContext testContext) {
        uploadingFile("testfile.jpg", "image/jpg", true, testContext);

    }

    @Test
    @DisplayName("Upload mc ppt file")
    @Order(2)
    void uploadExcelFile(Vertx vertx, VertxTestContext testContext) {
        uploadingFile("testfile.pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation", true, testContext);

    }

    @Test
    @DisplayName("Upload Text file")
    @Order(3)
    void uploadTextFile(Vertx vertx, VertxTestContext testContext) {
        uploadingFile("testfile.txt", "text/plain", true, testContext);

    }

    @Test
    @DisplayName("Upload Audio file")
    @Order(4)
    void uploadAudioFile(Vertx vertx, VertxTestContext testContext) {
        uploadingFile("testfile.mp3", "audio/mpeg", false, testContext);

    }


    @Test
    @DisplayName("Delete data")
    @Order(5)
    @Disabled
    void deleteTest(Vertx vertx, VertxTestContext testContext) {
        client.deleteAbs("http://localhost:8080/data/test")
                .bearerTokenAuthentication(token)
                .expect(ResponsePredicate.SC_NO_CONTENT)
                .send(ar -> {
                    if (ar.succeeded()) {
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar.cause());
                    }
                });
    }

    @Test
    @DisplayName("Upload data without ID")
    @Order(6)
    void uploadNoIDTest(Vertx vertx, VertxTestContext testContext) {
        MultipartForm form = MultipartForm.create();
        form.textFileUpload("data", "testfile.txt", "src/test/resources/testfile.txt", "text/plain");

        client.postAbs("http://localhost:8080/data?catalog=test")
                .bearerTokenAuthentication(token)
                .sendMultipartForm(form)
                .onSuccess(response -> {
                    if (response.statusCode() == 201) {
                        location = response.getHeader("Location");
                        log.debug(response.getHeader("Location"));
                        testContext.completeNow();
                    } else {
                        if (response.body() != null) {
                            log.debug(response.bodyAsString());
                        }
                        testContext.failNow(response.statusMessage());
                    }
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Download data")
    @Order(7)
    void downloadTest(Vertx vertx, VertxTestContext testContext) {
        client.getAbs("http://localhost:8080" + location)
                .expect(ResponsePredicate.SC_OK)
                .send()
                .onSuccess(response -> {
                    log.debug(response.bodyAsString());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Download data created without ID")
    @Order(8)
    void downloadTestID(Vertx vertx, VertxTestContext testContext) {
        if (Strings.isNullOrEmpty(location)) {
            testContext.failNow("no file location provided");
            return;
        }
        client.getAbs("http://localhost:8080" + location )
                .expect(ResponsePredicate.SC_OK)
                .send()
                .onSuccess(response -> {
                    log.debug("Download data created without ID: {}",response.bodyAsString());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }


    @Test
    @DisplayName("Delete data created without ID")
    @Order(9)
    void deleteTestID(Vertx vertx, VertxTestContext testContext) {
        if (Strings.isNullOrEmpty(location)) {
            testContext.failNow("no file location provided");
            return;
        }
        log.debug(location);
        client.deleteAbs("http://localhost:8080" + location+"?catalog=test")
                .bearerTokenAuthentication(token)
                .expect(ResponsePredicate.SC_NO_CONTENT)
                .send(ar -> {
                    if (ar.succeeded()) {
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar.cause());
                    }
                });
    }

    private void uploadingFile(String fileName, String contentType, Boolean valid, VertxTestContext testContext) {

        String id = UUID.randomUUID().toString();
        MultipartForm form = MultipartForm.create();
        form.binaryFileUpload("data", fileName, "src/test/resources/" + fileName, contentType);
        client.putAbs("http://localhost:8080/data/"+id+"?catalog=test")
                .bearerTokenAuthentication(token)
                .sendMultipartForm(form)
                .onSuccess(response -> {
                    if (response.statusCode() == 201) {
                        log.debug("Got response 201 Location  after uploading {}: {}", fileName, response.headers().get("Location"));
                        if (valid) {

                            testContext.completeNow();
                        } else {
                            testContext.failNow("'" + contentType + "' is not valid but succeeded!");
                        }
                    } else {
                        if (response.body() != null) {
                            log.debug("Got response after uploading {}: {}", fileName, response.bodyAsString());
                            log.debug("Got response status code after uploading {}: {}", fileName, response.statusCode());
                            log.debug("Got response Location  after uploading {}: {}", fileName, response.headers().get("Location"));
                        }
                        if (valid) {
                            testContext.failNow("'" + contentType + "' is valid but did not succeed!");
                        } else {
                            testContext.completeNow();
                        }
                    }
                })
                .onFailure(testContext::failNow);

    }


}
