package io.piveau.store;

import com.google.common.base.Strings;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.JWTOptions;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.ext.web.multipart.MultipartForm;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StoreApiTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private WebClient client;

    private String token;

    private MongodExecutable mongodExecutable = null;

    @BeforeAll
    void setup(Vertx vertx, VertxTestContext testContext) {
        try {
            MongodStarter starter = MongodStarter.getDefaultInstance();
            MongodConfig mongodConfig = MongodConfig.builder()
                    .version(Version.V5_0_2)
                    .net(new Net("127.0.0.1", 27018, false))
                    .build();
            mongodExecutable = starter.prepare(mongodConfig);
            mongodExecutable.start();
        } catch (Exception e) {
            testContext.failNow(e);
            return;
        }

        JWTAuthOptions jwtAuthOptions = new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                        .setAlgorithm("HS256")
                        .setBuffer("secret"));

        JWTAuth jwtAuth = JWTAuth.create(vertx, jwtAuthOptions);

        JsonObject authorization = new JsonObject().put("permissions", new JsonArray().add(
                new JsonObject()
                        .put("scopes", new JsonArray().add("dataset:update").add("dataset:delete").add("dataset:create"))
                        .put("rsname", "catalog-1")
        ));


        token = jwtAuth.generateToken(new JsonObject()
                .put("authorization", authorization)
                .put("realm_access", new JsonObject().put("roles", new JsonArray().add("provider"))), new JWTOptions()
                .setNoTimestamp(true)
                .setSubject("test"));
        log.info(token);
        JsonObject config = new JsonObject()
                .put(Constants.ENV_MONGO_DB_URI, "mongodb://localhost:27018")
                .put(Constants.ENV_STORAGE_BACKEND, "FILE")
                .put(Constants.ENV_PIVEAU_STORE_AUTH_CONFIG, new JsonObject().put("customSecret", "secret"))
                .put(Constants.ENV_MONGO_DB_NAME, "piveau");
        vertx.deployVerticle(io.piveau.store.MainVerticle.class, new DeploymentOptions().setConfig(config), ar -> {
            if (ar.succeeded()) {
                client = WebClient.create(vertx);
                testContext.completeNow();
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    @AfterAll
    void stop(Vertx vertx, VertxTestContext testContext) {
        mongodExecutable.stop();
        testContext.completeNow();
    }

    private String location = "";

    @Test
    @DisplayName("Download data")
    @Order(2)
    void downloadTest(Vertx vertx, VertxTestContext testContext) {
        client.getAbs("http://localhost:8080/data/test")
                .expect(ResponsePredicate.SC_OK)
                .send()
                .onSuccess(response -> {
                    log.debug(response.bodyAsString());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Delete data")
    @Order(3)
    void deleteTest(Vertx vertx, VertxTestContext testContext) {
        client.deleteAbs("http://localhost:8080/data/test")
                .bearerTokenAuthentication(token)
                .expect(ResponsePredicate.SC_ACCEPTED)
                .send(ar -> {
                    if (ar.succeeded()) {
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar.cause());
                    }
                });
    }

    @Test
    @DisplayName("Upload data")
    @Order(1)
    void uploadTest(Vertx vertx, VertxTestContext testContext) {
        MultipartForm form = MultipartForm.create();
        form.textFileUpload("data", "testfile.txt", "src/test/resources/testfile.txt", "text/plain");
        client.putAbs("http://localhost:8080/data/test?catalog=test")
                .bearerTokenAuthentication(token)
                .sendMultipartForm(form)
                .onSuccess(response -> {
                    if (response.statusCode() == 201) {
                        log.debug(response.getHeader("Location"));
                        testContext.completeNow();
                    } else {
                        if (response.body() != null) {
                            log.debug(response.bodyAsString());
                            System.out.println(response.headers().get("Location"));
                        }
                        testContext.failNow(response.statusMessage());
                    }
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Upload data without ID")
    @Order(4)
    void uploadNoIDTest(Vertx vertx, VertxTestContext testContext) {
        MultipartForm form = MultipartForm.create();
        form.textFileUpload("data", "testfile.txt", "src/test/resources/testfile.txt", "text/plain");
        client.postAbs("http://localhost:8080/data?catalog=test")
                .bearerTokenAuthentication(token)
                .sendMultipartForm(form)
                .onSuccess(response -> {
                    if (response.statusCode() == 201) {
                        location = response.getHeader("Location");
                        log.debug(response.getHeader("Location"));
                        testContext.completeNow();
                    } else {
                        if (response.body() != null) {
                            log.debug(response.bodyAsString());
                        }
                        testContext.failNow(response.statusMessage());
                    }
                })
                .onFailure(testContext::failNow);
    }


    @Test
    @DisplayName("Download data created without ID")
    @Order(5)
    void downloadTestID(Vertx vertx, VertxTestContext testContext) {
        if (Strings.isNullOrEmpty(location)) {
            testContext.failNow("no file location provided");
            return;
        }
        client.getAbs("http://localhost:8080" + location)
                .expect(ResponsePredicate.SC_OK)
                .send()
                .onSuccess(response -> {
                    log.debug(response.bodyAsString());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Delete data created without ID")
    @Order(6)
    void deleteTestID(Vertx vertx, VertxTestContext testContext) {
        if (Strings.isNullOrEmpty(location)) {
            testContext.failNow("no file location provided");
            return;
        }
        client.deleteAbs("http://localhost:8080" + location)
                .bearerTokenAuthentication(token)
                .expect(ResponsePredicate.SC_ACCEPTED)
                .send(ar -> {
                    if (ar.succeeded()) {
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar.cause());
                    }
                });
    }


}
