package io.piveau.store;

import com.google.common.base.Strings;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.piveau.store.db.DBHandler;
import io.piveau.store.storage.StorageBackend;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Testcontainers(disabledWithoutDocker = true)
class StoreTest {

    private DBHandler dbHandler;
    private DataManager dataManager;

    private final MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:4.4.11"));

    @BeforeAll
    void setup(Vertx vertx, VertxTestContext testContext) {
        mongoDBContainer.start();

        JsonObject dbConfig = new JsonObject()
                .put("connection_string", mongoDBContainer.getConnectionString())
                .put("db_name", "piveau");
        dbHandler = new DBHandler(MongoClient.create(vertx, dbConfig), "data");

        StorageBackend.create(vertx, new JsonObject()
                        .put(Constants.ENV_STORAGE_BACKEND, "FILE"))
                .onSuccess(sb -> {
                    dataManager = new DataManager(dbHandler, sb);
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @AfterAll
    void shutdown(Vertx vertx, VertxTestContext testContext) {
        vertx.fileSystem().exists(Constants.STORAGE_DEFAULT_LOCATION)
                .compose(exists -> {
                    if (exists) {
                        return vertx.fileSystem().deleteRecursive(Constants.STORAGE_DEFAULT_LOCATION, true);
                    } else {
                        return Future.succeededFuture();
                    }
                })
                .onSuccess(v -> testContext.completeNow())
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(1)
    @DisplayName("Save data")
    void saveData(Vertx vertx, VertxTestContext testContext) {
            dataManager.save("testfile.txt", "test.txt", 2000L, "1", "test")
                    .onSuccess(code -> {
                        testContext.verify(() -> Assertions.assertEquals(201, code));
                        testContext.completeNow();
                    })
                    .onFailure(testContext::failNow);
    }

    @Test
    @Order(2)
    @DisplayName("Update data")
    void updateData(Vertx vertx, VertxTestContext testContext) {
        dataManager.save("testfile.txt", "test.txt", 1234L, "1", "test")
                .onSuccess(code -> {
                    testContext.verify(() -> Assertions.assertEquals(200, code));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(3)
    @DisplayName("Find data")
    void findData(Vertx vertx, VertxTestContext testContext) {
        dbHandler.findDocument("1")
                .onSuccess(doc -> {
                    testContext.verify(() -> Assertions.assertEquals("1", doc.getString("_id")));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(4)
    @DisplayName("Delete data")
    void deleteData(Vertx vertx, VertxTestContext testContext) {
        dataManager.remove("1", "test")
                .onSuccess(json -> {
                    testContext.verify(() -> Assertions.assertEquals("1", json.getString("_id", "")));
                    vertx.setTimer(30, v -> {
                        testContext.completeNow();
                    });
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(5)
    @DisplayName("Find non existing data")
    void findDataNotFound(Vertx vertx, VertxTestContext testContext) {
        dbHandler.findDocument("1")
                .onSuccess(code -> testContext.failNow(new Throwable("Data should not exist")))
                .onFailure(cause -> {
                    testContext.verify(() -> Assertions.assertEquals(HttpResponseStatus.NOT_FOUND.reasonPhrase(), cause.getMessage()));
                    testContext.completeNow();
                });
    }

    @Test
    @Order(6)
    @DisplayName("Delete non existing data")
    void deleteDataNotFound(Vertx vertx, VertxTestContext testContext) {
        dataManager.remove("1" , "test")
                .onSuccess(s -> testContext.failNow("Removing non existing data somehow succeeded"))
                .onFailure(cause -> {
                    testContext.verify(() -> Assertions.assertEquals(HttpResponseStatus.NOT_FOUND.reasonPhrase(), cause.getMessage()));
                    testContext.completeNow();
                });
    }

    @Test
    @Order(7)
    @DisplayName("Save data without ID")
    void saveDataWithoutID(Vertx vertx, VertxTestContext testContext) {
        dataManager.save("testfile.txt", "test.txt", 1234L, "test")
                .onSuccess(id -> {
                    testContext.verify(() -> Assertions.assertFalse(Strings.isNullOrEmpty(id)));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }
}
