package io.piveau.store;

import com.google.common.base.Strings;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.piveau.store.filestorage.FilesystemStorageBackendImpl;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class StoreTest {

    private DBHandler dbHandler;
    private DataManager dataManager;


    private MongodExecutable mongodExecutable = null;

    @BeforeAll
    void setup(Vertx vertx, VertxTestContext testContext) {
        try {
            MongodStarter starter = MongodStarter.getDefaultInstance();
            MongodConfig mongodConfig = MongodConfig.builder()
                    .version(Version.V5_0_2)
                    .net(new Net("127.0.0.1", 27019, false))
                    .build();

            mongodExecutable = starter.prepare(mongodConfig);
            mongodExecutable.start();

            JsonObject config = new JsonObject()
                    .put("connection_string", "mongodb://localhost:27019")
                    .put("db_name", "piveau");
            dbHandler = new DBHandler(MongoClient.create(vertx, config), "data");
            dataManager = new DataManager(dbHandler,new FilesystemStorageBackendImpl(vertx), vertx.fileSystem().createTempDirectoryBlocking("store"));
            testContext.completeNow();
        } catch (Exception e) {
            testContext.failNow(e);
        }
    }

    @AfterAll
    void shutdown(Vertx vertx, VertxTestContext testContext) {
        dbHandler.cleanup().onComplete(v -> {
            mongodExecutable.stop();
            testContext.completeNow();
        });
    }

    @Test
    @Order(1)
    @DisplayName("Save data")
    void saveData(Vertx vertx, VertxTestContext testContext) {
        dataManager.save("testfile.txt", "test.txt", 2000L, "1", "test")
                .onSuccess(code -> {
                    testContext.verify(() -> Assertions.assertEquals(201, code));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(2)
    @DisplayName("Update data")
    void updateData(Vertx vertx, VertxTestContext testContext) {
        dataManager.save("testfile.txt", "test.txt", 1234L, "1", "test")
                .onSuccess(code -> {
                    testContext.verify(() -> Assertions.assertEquals(200, code));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(3)
    @DisplayName("Find data")
    void findData(Vertx vertx, VertxTestContext testContext) {
        dbHandler.findDocument("1")
                .onSuccess(doc -> {
                    testContext.verify(() -> Assertions.assertEquals("1", doc.getString("_id")));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(4)
    @DisplayName("Delete data")
    void deleteData(Vertx vertx, VertxTestContext testContext) {
        dataManager.remove("1")
                .onSuccess(json -> {
                    testContext.verify(() -> Assertions.assertEquals("1", json.getString("_id", "")));
                    vertx.setTimer(30, v -> {
                        testContext.completeNow();
                    });
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Order(5)
    @DisplayName("Find non existing data")
    void findDataNotFound(Vertx vertx, VertxTestContext testContext) {
        dbHandler.findDocument("1")
                .onSuccess(code -> testContext.failNow(new Throwable("Data should not exist")))
                .onFailure(cause -> {
                    testContext.verify(() -> Assertions.assertEquals(HttpResponseStatus.NOT_FOUND.reasonPhrase(), cause.getMessage()));
                    testContext.completeNow();
                });
    }

    @Test
    @Order(6)
    @DisplayName("Delete non existing data")
    void deleteDataNotFound(Vertx vertx, VertxTestContext testContext) {
        dataManager.remove("1")
                .onSuccess(s -> testContext.failNow("Removing non existing data somehow succeeded"))
                .onFailure(cause -> {
                    testContext.verify(() -> Assertions.assertEquals(HttpResponseStatus.NOT_FOUND.reasonPhrase(), cause.getMessage()));
                    testContext.completeNow();
                });
    }

    @Test
    @Order(1)
    @DisplayName("Save data without ID")
    void saveDataWithoutID(Vertx vertx, VertxTestContext testContext) {
        dataManager.save("testfile.txt", "test.txt", 1234L, "test")
                .onSuccess(id -> {
                    testContext.verify(() -> Assertions.assertFalse(Strings.isNullOrEmpty(id)));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }
}
