package io.piveau.store.filestorage;

import io.cloudonix.vertx.javaio.OutputToReadStream;
import io.cloudonix.vertx.javaio.WriteToInputStream;
import io.minio.*;
import io.minio.credentials.IamAwsProvider;
import io.minio.errors.*;
import io.minio.MinioClient.Builder;
import io.piveau.store.AsyncInputStream;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.WorkerExecutor;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.streams.WriteStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;


public class MinioStorageBackendImpl implements StorageBackend {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final MinioClient minioClient;
    private final Vertx vertx;
    private final WorkerExecutor executor;

    public MinioStorageBackendImpl(Vertx vertx, String endpoint, String accesskey, String secret, String region, String directory) {
        this.vertx = vertx;

        // Todo This WorkerExecutor is just a workaround. Refactoring to a real WorkerVerticle is advised.
        this.executor = vertx.createSharedWorkerExecutor("worker-pool", 4, 10, TimeUnit.MINUTES);

        Builder minioClientBuilder = MinioClient.builder();
        minioClientBuilder.endpoint(endpoint);

        if(region != null) {
            minioClientBuilder.region(region);
        }

        if(secret != null && accesskey != null) {
            minioClientBuilder.credentials(accesskey, secret);
        } else {
            minioClientBuilder.credentialsProvider(new IamAwsProvider(null, null));
        }

        this.minioClient = minioClientBuilder.build();

        executor.executeBlocking(a -> {
            try {
                this.minioClient.bucketExists(BucketExistsArgs.builder().bucket(directory).build());
                System.out.println("Successfully connected to S3");
            } catch (Exception e) {
                System.out.println("ERROR when connecting to S3: " + e.getMessage());
            }
        });

    }

    public Future<Void> createDirectory(String directory) {
        return createDirectoryIfNotExists(directory);
    }

    public Future<Void> removeDirectory(String directory) {
        Promise<Void> promise = Promise.promise();

        directoryExists(directory).onFailure(promise::fail).onSuccess(exist -> {
            if (exist) {
                executor.executeBlocking(a -> {
                    try {
                        minioClient.removeBucket(RemoveBucketArgs.builder().bucket(directory).build());
                        promise.complete();
                    } catch (ServerException | ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | XmlParserException e) {
                        e.printStackTrace();
                        a.fail(e);
                    }
                }, result -> {
                    if (result.failed()) {
                        promise.fail(result.cause());
                    } else {
                        promise.complete();
                    }
                });

            } else {
                promise.fail("Bucket does not exist");

            }
        });

        return promise.future();

    }


    public Future<Void> createDirectoryIfNotExists(String directory) {
        System.out.println("directory = " + directory);
        Promise<Void> promise = Promise.promise();

        directoryExists(directory).onFailure(promise::fail).onSuccess(exist -> {
            if (exist) {
                promise.complete();
            } else {
                executor.executeBlocking(a -> {
                    try {
                        System.out.println("creating bucket");
                        minioClient.makeBucket(MakeBucketArgs.builder().bucket(directory).build());
                        a.complete();
                    } catch (ServerException | ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | XmlParserException e) {
                        e.printStackTrace();
                        a.fail(e);
                    }
                }, result -> {
                    if (result.failed()) {
                        promise.fail(result.cause());
                    } else {
                        promise.complete();
                    }
                });
            }
        });

        return promise.future();
    }


    public Future<Boolean> directoryExists(String directory) {
        Promise<Boolean> promise = Promise.promise();
        executor.executeBlocking(a -> {
            try {
                // Create a InputStream for object upload.
                Boolean exists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(directory).build());
                a.complete(exists);
            } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
                a.fail(e);
            }
        }, result -> {
            if (result.failed()) {
                promise.fail(result.cause());
            } else {
                promise.complete((Boolean) result.result());
            }
        });
        return promise.future();
    }


    public Future<Void> putFile(String directory, String name, String uploadname) {
        System.out.println("directory = " + directory + ", name = " + name + ", uploadname = " + uploadname);
        Promise<Void> promise = Promise.promise();
        executor.executeBlocking(a -> {
            try {
                minioClient.uploadObject(
                        UploadObjectArgs.builder()
                                .bucket(directory)
                                .object(name)
                                .filename(uploadname)
                                .build());

                vertx.fileSystem().deleteBlocking(uploadname);
                a.complete();
            } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
                vertx.fileSystem().exists(uploadname).onSuccess(exists -> {
                    if (exists) vertx.fileSystem().deleteBlocking(uploadname);
                });
                e.printStackTrace();
                a.fail(e);
            }
        }, result -> {
            if (result.failed()) {
                promise.fail(result.cause());
            } else {
                promise.complete();
            }
        });
        return promise.future();
    }


    public Future<Void> getFile(String container, String name, WriteStream<Buffer> writeStream) {
        Promise<Void> promise = Promise.promise();
        executor.executeBlocking(a -> {
            try {
                InputStream stream = minioClient.getObject(GetObjectArgs.builder().bucket(container).object(name).build());
                AsyncInputStream asyncInputStream = new AsyncInputStream(vertx, vertx.getOrCreateContext(), stream)
                        .exceptionHandler(promise::fail);
                asyncInputStream.pipeTo(writeStream);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                promise.fail(e);
            }
        });
        return promise.future();
    }


    public Future<Void> removeFile(String directory, String name) {
        Promise<Void> promise = Promise.promise();

        executor.executeBlocking(a -> {
            try {
                minioClient.removeObject(RemoveObjectArgs.builder().bucket(directory).object(name).build());
                a.complete();
            } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
                a.fail(e);
            }
        }, result -> {
            if (result.failed()) {
                promise.fail(result.cause());
            } else {
                promise.complete();
            }
        });
        return promise.future();
    }

}
