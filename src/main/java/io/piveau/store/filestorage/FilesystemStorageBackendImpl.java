package io.piveau.store.filestorage;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.CopyOptions;
import io.vertx.core.file.FileSystem;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.streams.WriteStream;

import java.io.File;

public class FilesystemStorageBackendImpl implements StorageBackend {

    private final FileSystem fs;

    public FilesystemStorageBackendImpl(Vertx vertx) {
        fs = vertx.fileSystem();
    }

    @Override
    public Future<Void> createDirectory(String directory) {
        Promise<Void> promise = Promise.promise();
        fs.mkdir(directory, promise);
        return promise.future();
    }


    @Override
    public Future<Void> createDirectoryIfNotExists(String directory) {
        Promise<Void> promise = Promise.promise();
        directoryExists(directory).onFailure(promise::fail).onSuccess(exist -> {
            if (exist) {
                promise.complete();
            } else {
                createDirectory(directory).onComplete(promise);
            }
        });
        return promise.future();
    }

    @Override
    public Future<Void> removeDirectory(String directory) {
        Promise<Void> promise = Promise.promise();
        fs.delete(directory, promise);
        return promise.future();
    }

    @Override
    public Future<Boolean> directoryExists(String directory) {

        Promise<Boolean> promise = Promise.promise();
        fs.exists(directory, promise);
        return promise.future();


    }

    @Override
    public Future<Void> putFile(String directory, String name, String uploadname) {

        Promise<Void> promise = Promise.promise();
        File dir = new File(directory);
        File file = new File(dir, name);

        fs.move(uploadname, file.getPath(), new CopyOptions().setReplaceExisting(true), promise);
        //fs.writeFile(file.getPath(), Buffer.buffer(bytes), promise);
        return promise.future();
    }

    @Override
    public Future<Void> getFile(String directory, String name, WriteStream<Buffer> writeStream) {

        Promise<Void> promise = Promise.promise();
        File dir = new File(directory);

        File file = new File(dir, name);

        fs.open(file.getPath(), new OpenOptions().setCreate(false).setRead(true).setWrite(false)).onFailure(promise::fail).onSuccess(afile -> afile.pipeTo(writeStream).onFailure(promise::fail).onSuccess(promise::complete));
        return promise.future();
    }

    @Override
    public Future<Void> removeFile(String directory, String name) {
        Promise<Void> promise = Promise.promise();
        File dir = new File(directory);
        File file = new File(dir, name);
        fs.delete(file.getPath())
                .onSuccess(promise::complete)
                .onFailure(err -> {

                });
        return promise.future();
    }

}
