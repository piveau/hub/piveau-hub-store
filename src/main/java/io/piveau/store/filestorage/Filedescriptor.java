package io.piveau.store.filestorage;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Filedescriptor {

    private String filename;
    private Long size;
    private ZonedDateTime lastModified;


    public Filedescriptor(String filename, Long size, ZonedDateTime lastModified) {
        this.filename = filename;
        this.size = size;
        this.lastModified = lastModified;
    }

    public Filedescriptor(String filename, long size, long lastModifiedTime) {
        this.filename = filename;
        this.size = size;
        this.lastModified = ZonedDateTime.ofInstant(Instant.ofEpochSecond(lastModifiedTime), ZoneId.systemDefault());
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public ZonedDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(ZonedDateTime lastModified) {
        this.lastModified = lastModified;
    }
}
