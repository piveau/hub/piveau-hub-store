package io.piveau.store.filestorage;

import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.streams.WriteStream;

public interface StorageBackend {


    Future<Void> createDirectory(String directory);

    Future<Void> createDirectoryIfNotExists(String directory);

    Future<Void> removeDirectory(String directory);

    Future<Boolean> directoryExists(String directory);

    Future<Void> putFile(String directory, String name, String uploadedName);

    Future<Void> getFile(String directory, String name, WriteStream<Buffer> writeStream);

    Future<Void> removeFile(String directory, String name);

}
