package io.piveau.store;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;
import io.vertx.ext.mongo.WriteOption;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

public class DBHandler {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final MongoClient mongoClient;
    private final String dbName;


    public DBHandler(MongoClient mongoClient, String dbName) {
        this.mongoClient = mongoClient;
        this.dbName = dbName;
    }

    public Future<Integer> upsertDocument(String fileName, String id, Long fileSize, String catalog) {

        Promise<Integer> promise = Promise.promise();
        JsonObject query = new JsonObject().put("_id", id);
        JsonObject document = new JsonObject()
                .put("_id", id)
                .put("fileName", fileName)
                .put("fileSize", fileSize)
                .put("fileLastModified", Instant.now().getEpochSecond())
                .put("catalog", catalog);

        mongoClient.replaceDocumentsWithOptions(dbName, query, document, new UpdateOptions().setUpsert(true), ar -> {
            if (ar.succeeded()) {
                promise.complete(ar.result().getDocUpsertedId() != null ? 201 : 200);
            } else {
                promise.fail(ar.cause());
            }
        });
        return promise.future();
    }


    public Future<String> insertDocument(String fileName, Long fileSize, String catalog) {

        Promise<String> promise = Promise.promise();

        JsonObject document = new JsonObject()
                .put("fileName", fileName)
                .put("fileSize", fileSize)
                .put("fileLastModified", Instant.now().getEpochSecond())
                .put("catalog", catalog);

        mongoClient.save(dbName, document).onFailure(promise::fail).onSuccess(promise::complete);

        return promise.future();
    }

    public Future<Integer> removeDocument(String id) {
        Promise<Integer> promise = Promise.promise();

        JsonObject document = new JsonObject().put("_id", id);
        mongoClient.removeDocumentWithOptions(dbName, document, WriteOption.ACKNOWLEDGED, res -> {
            if (res.succeeded()) {
                promise.complete(res.result().getRemovedCount() == 1 ? 204 : 404);
            } else {
                promise.fail(res.cause());
            }
        });


        return promise.future();
    }


    public Future<JsonObject> findDocument(String id) {
        Promise<JsonObject> promise = Promise.promise();
        JsonObject queryDocument = new JsonObject().put("_id", id);
        mongoClient.findOne(dbName, queryDocument, null, ar -> {
            if (ar.succeeded()) {
                if (ar.result() != null) {
                    promise.complete(ar.result());
                } else {
                    promise.fail(new ServiceException(404, HttpResponseStatus.NOT_FOUND.reasonPhrase()));
                }
            } else {
                promise.fail(ar.cause());
            }
        });
        return promise.future();
    }

    public Future<Void> cleanup() {
        Promise<Void> promise = Promise.promise();
        mongoClient.dropCollection(dbName, promise);
        return promise.future();
    }

}
