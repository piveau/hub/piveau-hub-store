package io.piveau.store;

import io.piveau.store.db.DBHandler;
import io.piveau.store.db.FileDescriptor;
import io.piveau.store.storage.StorageBackend;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.WriteStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class DataManager {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final DBHandler dbHandler;

    private final StorageBackend storageBackend;

    public DataManager(DBHandler dbHandler, StorageBackend storageBackend) {
        this.dbHandler = dbHandler;
        this.storageBackend = storageBackend;
    }

    public Future<String> save(String uploadedName, String fileName, Long fileSize, String catalog) {
        Map<String, String> metadata = Map.of("Filename", fileName, "Catalogue", catalog);

        return dbHandler.insertDocument(fileName, fileSize, catalog)
                .compose(id -> store(id, uploadedName, metadata));
    }

    public Future<Integer> save(String uploadedName, String fileName, Long fileSize, String id, String catalog) {
        Map<String, String> metadata = Map.of("Filename", fileName, "Catalogue", catalog);

        return dbHandler.upsertDocument(fileName, id, fileSize, catalog)
                .onSuccess(statusCode -> store(id, uploadedName, metadata));
    }

    public Future<JsonObject> remove(String id , String catalog) {
        return dbHandler.findDocumentForRemove(id, catalog)
                .onSuccess(json -> storageBackend.removeFile(id)
                        .compose(v -> dbHandler.removeDocument(id, catalog))
                        .onSuccess(v -> log.debug("Successfully deleted file: {}", id))
                        .onFailure(err -> log.warn("could not delete file: {}", id)));
    }

    public Future<Void> getFile(String id, WriteStream<Buffer> writeStream) {
        return dbHandler.findDocument(id)
                .compose(doc -> storageBackend.getFile(id, writeStream));
    }

    public Future<FileDescriptor> getName(String id) {
        return dbHandler.findDocument(id)
                .map(doc -> new FileDescriptor(doc.getString("fileName", "NoName"), doc.getLong("fileSize", -1L), doc.getLong("fileLastModified", 0L)));
    }

    private Future<String> store(String id, String uploadedName, Map<String, String> metadata) {
        String catalog = metadata.get( "Catalogue" );
        return storageBackend.putFile(id, uploadedName, metadata)
                .onFailure(cause -> {
                    log.error("Could not put the file to the storage backend", cause);
                    dbHandler.removeDocument(id , catalog);
                })
                .map(id);
    }

}
