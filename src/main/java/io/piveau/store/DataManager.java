package io.piveau.store;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.piveau.store.filestorage.Filedescriptor;
import io.piveau.store.filestorage.StorageBackend;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.WriteStream;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.NoSuchFileException;

public class DataManager {


    private final Logger log = LoggerFactory.getLogger(getClass());


    private final DBHandler dbHandler;

    private final StorageBackend storageBackend;
    private final String defaultDirectory;

    public DataManager(DBHandler dbHandler, StorageBackend storageBackend, String defaultDirectory) {
        this.dbHandler = dbHandler;
        this.storageBackend = storageBackend;
        this.defaultDirectory = defaultDirectory;

        storageBackend.createDirectoryIfNotExists(defaultDirectory);
    }

    public Future<String> save(String uploadedName, String fileName, Long fileSize, String catalog) {

        Promise<String> promise = Promise.promise();

        dbHandler.insertDocument(fileName, fileSize, catalog)
                .onFailure(promise::fail)
                .onSuccess(id -> storageBackend.putFile(defaultDirectory, id, uploadedName)
                        .onFailure(err -> {
                            //if file could not be written, we delete from db
                            log.error("not able to save file in storage, revert db. id: " + id + " filename: " + fileName);
                            dbHandler.removeDocument(id)
                                    .onSuccess(sc -> {
                                        if (sc == 404) {
                                            log.warn("trying to revert saving to db but document not found: " + id);
                                        }
                                        promise.fail(err);
                                    })
                                    .onFailure(promise::fail);
                        }).onSuccess(v -> promise.complete(id)));
        return promise.future();
    }


    public Future<Integer> save(String uploadedName, String fileName, Long fileSize, String id, String catalog) {
        Promise<Integer> promise = Promise.promise();

        dbHandler.upsertDocument(fileName, id, fileSize, catalog)
                .onFailure(promise::fail)
                .onSuccess(statuscode -> storageBackend.putFile(defaultDirectory, id, uploadedName)
                        .onFailure(err -> {
                            //if file could not be written, we delete from db
                            log.error("not able to save file in storage, revert db. id: " + id + " filename: " + fileName);
                            dbHandler.removeDocument(id)
                                    .onSuccess(sc -> {
                                        if (sc == 404) {
                                            log.warn("trying to revert saving to db but document not found: " + id);
                                        }
                                        promise.fail(err);
                                    })
                                    .onFailure(promise::fail);
                        }).onSuccess(v -> promise.complete(statuscode)));


        return promise.future();
    }

    public Future<JsonObject> remove(String id) {
        Promise<JsonObject> promise = Promise.promise();

        dbHandler.findDocument(id)
                .onSuccess(json -> {
                    promise.complete(json);
                    storageBackend.removeFile(defaultDirectory, id)
                            .compose(v -> dbHandler.removeDocument(id))
                            .onSuccess(v -> log.debug("Successfully deleted file: " + id))
                            .onFailure(err -> log.warn("could not delete file: " + id));


                })
                .onFailure(promise::fail);

        return promise.future();
    }


    public Future<Void> getFile(String id, WriteStream<Buffer> writeStream) {
        Promise<Void> promise = Promise.promise();
        dbHandler.findDocument(id)
                .compose(doc -> storageBackend.getFile(defaultDirectory, id, writeStream))
                .onSuccess(promise::complete).onFailure(err -> {
                    if (err instanceof NoSuchFileException) {
                        promise.fail(new ServiceException(500, HttpResponseStatus.NOT_FOUND.reasonPhrase()));
                    } else if (err instanceof io.minio.errors.ErrorResponseException && ((io.minio.errors.ErrorResponseException) err).errorResponse().code().equals("NoSuchKey")) {


                        promise.fail(new ServiceException(500, HttpResponseStatus.NOT_FOUND.reasonPhrase()));
                    } else {

                        promise.fail(err);

                    }

                });


        return promise.future();
    }

    public Future<Filedescriptor> getName(String id) {
        Promise<Filedescriptor> promise = Promise.promise();
        dbHandler.findDocument(id)
                .onSuccess(doc -> {
                    Filedescriptor fd = new Filedescriptor(doc.getString("fileName", "NoName"), doc.getLong("fileSize", -1L), doc.getLong("fileLastModified", 0L));
                    promise.complete(fd);
                })
                .onFailure(promise::fail);


        return promise.future();
    }

}
