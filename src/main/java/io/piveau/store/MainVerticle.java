package io.piveau.store;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.piveau.json.ConfigHelper;
import io.piveau.security.PiveauAuth;
import io.piveau.security.PiveauAuthConfig;
import io.piveau.store.db.DBHandler;
import io.piveau.store.security.PermissionHandler;
import io.piveau.store.security.VirusScanner;
import io.piveau.store.storage.StorageBackend;
import io.piveau.utils.ConfigurableAssetHandler;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Launcher;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.validation.BadRequestException;
import io.vertx.ext.web.validation.RequestParameters;
import io.vertx.ext.web.validation.ValidationHandler;
import io.vertx.serviceproxy.ServiceException;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.capybara.clamav.ClamavException;
import xyz.capybara.clamav.CommunicationException;
import xyz.capybara.clamav.commands.scan.result.ScanResult;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

public class MainVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private DataManager dataManager;

    private JsonObject buildInfo;
    private VirusScanner virusScanner;
    private JsonObject config = new JsonObject();

    public static void main(String[] args) {
        Launcher.executeCommand("run", MainVerticle.class.getName());
    }

    @Override
    public void start(Promise<Void> promise) {

        vertx.fileSystem().readFile("buildInfo.json")
                .onSuccess(buffer -> buildInfo = buffer.toJsonObject())
                .onFailure(t -> buildInfo = new JsonObject()
                        .put("timestamp", "Build time not available")
                        .put("version", "Version not available"))
                .transform(ar -> loadConfig())
                .compose(this::setupBackend)
                .compose(this::startServer)
                .onSuccess(v -> {
                    log.info("piveau data store successfully launched");
                    promise.complete();
                })
                .onFailure(promise::fail);
    }

    private Future<JsonObject> loadConfig() {
        return ConfigRetriever.create(vertx).getConfig()
                .onSuccess(config -> log.debug("Successfully loaded configuration."));
    }

    private Future<JsonObject> setupBackend(JsonObject config) {


        boolean virusScannerEnabled = config.getBoolean(Constants.ENV_CLAMAV_ENABLED, Constants.DEFAULT_CLAMAV_ENABLED);

        if (virusScannerEnabled) {
            log.info("Virus Scanner enabled");
            String virusScannerHost = config.getString(Constants.ENV_CLAMAV_HOST, Constants.DEFAULT_CLAMAV_HOST);
            int virusScannerPort = config.getInteger(Constants.ENV_CLAMAV_PORT, Constants.DEFAULT_CLAMAV_PORT);
            virusScanner = VirusScanner.create(virusScannerHost, virusScannerPort);
            if (virusScanner == null) {
                return Future.failedFuture("Virus scanner not reachable under " + virusScannerHost + ":" + virusScannerPort);
            }
        } else {
            log.info("Virus Scanner disabled");
            virusScanner = null;
        }


        Future<StorageBackend> storageBackendFuture = StorageBackend.create(vertx, config)
                .onSuccess(sb -> log.debug("Successfully initialized storage backend of type {}", sb.type()))
                .onFailure(cause -> log.error("Storage backend", cause));

        Future<DBHandler> dbHandlerFuture = DBHandler.create(vertx, config)
                .onSuccess(dbh -> log.debug("Successfully initialized metadata database"))
                .onFailure(cause -> log.error("Metadata database", cause));

        return Future.all(storageBackendFuture, dbHandlerFuture)
                .onSuccess(cf -> dataManager = new DataManager(dbHandlerFuture.result(), storageBackendFuture.result()))
                .map(config);
    }

    private Future<Void> startServer(JsonObject config) {
        this.config = config;
        Promise<Void> promise = Promise.promise();
        JsonObject authConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_STORE_AUTH_CONFIG);
        if(authConfig.isEmpty()){
            log.warn("No authentication configured");
        }
        RouterBuilder.create(vertx, "webroot/openapi.yaml")
                .onSuccess(builder -> {
                    PiveauAuth.create(vertx, new PiveauAuthConfig(authConfig))
                            .compose(piveauAuth -> {
                                WebClient client = WebClient.create(vertx);

                                Set<String> allowedHeaders = Set.of(
                                        "x-requested-with",
                                        "Content-Type",
                                        "Accept",
                                        "Authorization");

                                Set<HttpMethod> allowedMethods = Set.of(
                                        HttpMethod.GET,
                                        HttpMethod.OPTIONS,
                                        HttpMethod.DELETE,
                                        HttpMethod.PUT,
                                        HttpMethod.POST
                                );


                                builder.securityHandler("BearerAuth", piveauAuth.authHandler());

                                //                                String corsDomains = "^(https?:\\/\\/(?:.+\\.)?(?:fokus\\.fraunhofer\\.de|localhost|data\\.europa\\.eu)(?::\\d{1,5})?)$";
                                List<String> corsDomains = config.getJsonArray(Constants.ENV_PIVEAU_STORE_CORS_DOMAINS, new JsonArray().add("*")).stream().map(Object::toString).toList();
                                builder.rootHandler(CorsHandler.create().addOrigins(corsDomains).allowedHeaders(allowedHeaders).allowedMethods(allowedMethods).allowCredentials(true));

                                builder.rootHandler(BodyHandler.create().setBodyLimit(-1));

                                builder.rootHandler(StaticHandler.create());

                                builder.operation("getData").handler(this::handleGetData);

                                builder.operation("putData")
                                        .handler(new PermissionHandler(List.of(
                                                Constants.AUTH_SCOPE_DATASET_CREATE,
                                                Constants.AUTH_SCOPE_DATASET_UPDATE)))
                                        .handler(this::handlePutData);

                                builder.operation("postData")
                                        .handler(new PermissionHandler(List.of(
                                                Constants.AUTH_SCOPE_DATASET_CREATE,
                                                Constants.AUTH_SCOPE_DATASET_UPDATE)))
                                        .handler(this::handlePostData);

                                builder.operation("deleteData")
                                        .handler(new PermissionHandler(List.of(
                                                Constants.AUTH_SCOPE_DATASET_DELETE,
                                                Constants.AUTH_SCOPE_DATASET_UPDATE)))
                                        .handler(this::handleDeleteData);

                                Router router = builder.createRouter();

                                router.route("/images/logo").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_LOGO_PATH, "webroot/images/logo.png"), client));
                                router.route("/images/favicon").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_FAVICON_PATH, "webroot/images/favicon.png"), client));
                                router.route("/imprint").handler(context ->
                                        context.redirect(config.getString(Constants.ENV_PIVEAU_IMPRINT_URL, "/")));

                                router.route("/privacy").handler(context ->
                                        context.redirect(config.getString(Constants.ENV_PIVEAU_PRIVACY_URL, "/")));

                                HealthCheckHandler hch = HealthCheckHandler.create(vertx);
                                hch.register("buildInfo", status -> status.complete(Status.OK(buildInfo)));
                                router.get("/health").handler(hch);

                                router.errorHandler(400, context -> {
                                    if (context.failure() instanceof BadRequestException failure) {
                                        JsonObject error = new JsonObject()
                                                .put("status", "error")
                                                .put("message", failure.getMessage());
                                        context.response().setStatusCode(400).putHeader("Content-Type", "application/json").end(error.encodePrettily());
                                    }
                                });

                                return vertx
                                        .createHttpServer(new HttpServerOptions().setPort(config.getInteger(Constants.ENV_HTTP_PORT, 8080)))
                                        .requestHandler(router)
                                        .listen();
                            })
                            .onSuccess(server -> {
                                log.info("Listening on Port " + server.actualPort());
                                promise.complete();
                            })
                            .onFailure(promise::fail);

                }).onFailure(promise::fail);

        return promise.future();
    }

    private void handlePostData(RoutingContext context) {
        if (context.fileUploads().size() != 1) {
            context.response().setStatusCode(400).end(failureJson("Only one file allowed in upload", 400));
            log.error("Number of files uploaded: {}", context.fileUploads().size());
            return;
        }


        FileUpload file = context.fileUploads().get(0);


        try {
            if (!isAllowedType(file)) {
                context.response().setStatusCode(400).end("Unsupported file type for upload. Please upload a valid file type.");
                return;
            }
        } catch (IOException e) {
            log.error("error reading file", e);
            context.response().setStatusCode(500).end("Error reading uploaded file");
            return;
        }

        if (virusScanner != null) {
            try {

                ScanResult result = virusScanner.scanFile(file);
                if (result instanceof ScanResult.VirusFound) {
                    context.response().setStatusCode(400).end();
                } else {
                    handlePostHelper(context, file);
                }
            } catch (IOException e) {
                log.error("File IO", e);
                context.response().setStatusCode(500).end();
            } catch (ClamavException e) {
                Throwable embedded = e.getCause();
                if (embedded instanceof CommunicationException) {
                    log.error("Scanning file", embedded);
                } else {
                    log.error("Scanning file", embedded.getCause());
                }
                context.response().setStatusCode(500).end();
            }
        } else {
            handlePostHelper(context, file);
        }

    }


    private void handlePostHelper(RoutingContext context, FileUpload file) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        dataManager.save(file.uploadedFileName(), file.fileName(), file.size(), parameters.queryParameter("catalog").getString())
                .onSuccess(id -> context.response()
                        .setStatusCode(201)
                        .putHeader("Location", "/data/" + id)
                        .end(successJson(false, file, id)))
                .onFailure(cause -> context.response().setStatusCode(500).end(failureJson(cause.getMessage(), 500)));

    }

    private void handlePutData(RoutingContext context) {
        log.debug("Enter put data");
        if (context.fileUploads().size() != 1) {
            context.response().setStatusCode(400).end(failureJson("Only one file allowed in upload", 400));
            log.error("Number of files uploaded: {}", context.fileUploads().size());
            return;
        }

        FileUpload file = context.fileUploads().get(0);
        log.debug("Upload of {}", file.fileName());

        try {
            if (!isAllowedType(file)) {
                context.response().setStatusCode(400).end("Unsupported file type for upload. Please upload a valid file type.");
                return;
            }
        } catch (IOException e) {
            log.error("error reading file", e);
            context.response().setStatusCode(500).end("Error reading uploaded file");
            return;
        }

        if (virusScanner != null) {
            try {
                ScanResult result = virusScanner.scanFile(file);
                if (result instanceof ScanResult.VirusFound) {
                    context.response().setStatusCode(400).end();
                } else {
                    handlePutHelper(context, file);
                }
            } catch (IOException e) {
                log.error("File IO", e);
                context.response().setStatusCode(500).end();
            } catch (ClamavException e) {
                Throwable embedded = e.getCause();
                if (embedded instanceof CommunicationException) {
                    log.error("Scanning file", embedded);
                } else {
                    log.error("Scanning file", embedded.getCause());
                }
                context.response().setStatusCode(500).end();
            }
        } else {
            handlePutHelper(context, file);
        }
    }

    private void handlePutHelper(RoutingContext context, FileUpload file) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        dataManager.save(file.uploadedFileName(), file.fileName(), file.size(), parameters.pathParameter("id").getString(), parameters.queryParameter("catalog").getString())
                .onSuccess(code -> context.response()
                        .setStatusCode(code)
                        .putHeader("Location", "/data/" + parameters.pathParameter("id").getString())
                        .end(successJson(code != 201, file, parameters.pathParameter("id").getString())))
                .onFailure(cause -> context.response().setStatusCode(500).end(failureJson(cause.getMessage(), 500)));

    }


    private Boolean isAllowedType(FileUpload file) throws IOException {
        String mimeType = file.contentType();
        JsonArray allowedMimeTypes = ConfigHelper.forConfig(config).forceJsonArray(Constants.ENV_ALLOWED_MIME_TYPES);
        if (allowedMimeTypes.isEmpty()) {
            return true;
        }

        allowedMimeTypes.add("application/octet-stream");
        log.info("Allowed mime types: {}", allowedMimeTypes.encodePrettily());
        log.info("Mime Type:{}", mimeType);


        //if the mime type does not match any of the allowed one, return false (file is not allowed)
        if (allowedMimeTypes.stream().noneMatch(allowedType -> mimeType.startsWith(allowedType.toString()))) {
            return false;
        }


        File upload = new File(file.uploadedFileName());


        TikaConfig config = TikaConfig.getDefaultConfig();
        Detector detector = config.getDetector();

        Metadata metadata = new Metadata();
        metadata.set(TikaCoreProperties.RESOURCE_NAME_KEY, file.fileName());
        try (InputStream is = TikaInputStream.get(upload.toPath(), metadata)) {
            MediaType mediatype = detector.detect(is, metadata);

            log.info("detected media type: {}", mediatype);
            if (allowedMimeTypes.stream().noneMatch(allowedType -> mediatype.toString().startsWith(allowedType.toString())))
                return false;

        } catch (IOException e) {
            log.error("Could not read uploaded file", e);
            throw e;
        }

        log.debug("is allowed");


        // if the extension matches any of the allowed ones, return true, else false
        return true;
    }


    private String successJson(boolean updated, FileUpload fileUpload, String id) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.put("success", true);
        JsonObject result = new JsonObject();
        result.put("filename", fileUpload.fileName());
        result.put("id", id);
        result.put("location", "/data/" + id);
        result.put("size", fileUpload.size());
        result.put("mimetype", fileUpload.contentType());
        result.put("updated", updated);
        jsonObject.put("result", result);

        return jsonObject.encodePrettily();
    }


    private String successJson(String id) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.put("success", true);
        JsonObject result = new JsonObject();
        result.put("id", id);
        jsonObject.put("result", result);

        return jsonObject.encodePrettily();
    }

    private String failureJson(String error, int code) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.put("success", false);

        if (!error.isEmpty()) {
            jsonObject.put("code", code);
            jsonObject.put("error", error);
        }

        return jsonObject.encodePrettily();
    }


    private void handleDeleteData(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String catalog = parameters.queryParameter("catalog").getString();
        String id = parameters.pathParameter("id").getString();
        dataManager.remove(id, catalog)
                .onSuccess(a -> context.response().setStatusCode(HttpResponseStatus.NO_CONTENT.code())
                        .end("this id has been deleted successfully" + id + catalog))
                .onFailure(cause -> failureResponse(context, cause));
    }

    private void handleGetData(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String id = parameters.pathParameter("id").getString();
        dataManager.getName(id)
                .compose(fileDescriptor -> {
                    context.response()
                            .putHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDescriptor.getFilename() + "\"")
                            .setStatusCode(200).setChunked(true);

                    return dataManager.getFile(id, context.response());
                })
                .onSuccess(v -> context.response())
                .onFailure(cause -> {
                    log.error(cause.getMessage());
                    if (!context.response().headWritten()) {
                        context.response().putHeader(HttpHeaders.CONTENT_DISPOSITION, "");
                        context.response().setChunked(false);
                    }
                    if (cause instanceof ServiceException se) {
                        context.response().setStatusCode(se.failureCode());
                        context.response().end(failureJson(se.getMessage(), se.failureCode()));
                    } else {
                        if (cause.getMessage() != null) {
                            int code = cause.getMessage().equals(HttpResponseStatus.NOT_FOUND.reasonPhrase()) ? 404 : 500;
                            context.response().setStatusCode(code).end(failureJson(cause.getLocalizedMessage(), code));
                        } else {
                            context.fail(500);
                        }
                    }
                });
    }

    private void failureResponse(RoutingContext routingContext, Throwable cause) {
        if (cause instanceof ServiceException se) {
            routingContext.response().setStatusCode(se.failureCode());
            if (se.getDebugInfo() != null) {
                routingContext.response().end(se.getDebugInfo().encodePrettily());
            } else {
                routingContext.response().end(se.getMessage());
            }
        } else {
            routingContext.fail(500, cause);
        }
    }

}
