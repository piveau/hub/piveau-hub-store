package io.piveau.store;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.piveau.json.ConfigHelper;
import io.piveau.security.PiveauAuth;
import io.piveau.security.PiveauAuthConfig;
import io.piveau.store.filestorage.FilesystemStorageBackendImpl;
import io.piveau.store.filestorage.MinioStorageBackendImpl;
import io.piveau.store.filestorage.StorageBackend;
import io.piveau.utils.ConfigurableAssetHandler;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Launcher;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.validation.BadRequestException;
import io.vertx.ext.web.validation.RequestParameters;
import io.vertx.ext.web.validation.ValidationHandler;
import io.vertx.serviceproxy.ServiceException;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class MainVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private DataManager dataManager;

    private JsonObject buildInfo;

    public static void main(String[] args) {
        Launcher.executeCommand("run", MainVerticle.class.getName());
    }

    @Override
    public void start(Promise<Void> promise) {

        Buffer buffer = vertx.fileSystem().readFileBlocking("buildInfo.json");
        if (buffer != null) {
            buildInfo = buffer.toJsonObject();
        } else {
            buildInfo = new JsonObject()
                    .put("timestamp", "Build time not available")
                    .put("version", "Version not available");
        }

        loadConfig()
                .compose(this::setupBackend)
                .compose(this::startServer)
                .onSuccess(v -> {
                    log.info("Data uploader successfully launched");
                    promise.complete();
                })
                .onFailure(promise::fail);
    }

    private Future<JsonObject> loadConfig() {
        Promise<JsonObject> promise = Promise.promise();

        ConfigRetriever.create(vertx).getConfig(ar -> {
            if (ar.succeeded()) {
                log.debug("Successfully loaded configuration.");
                promise.complete(ar.result());
            } else {
                promise.fail(ar.cause());
            }
        });

        return promise.future();
    }


    private Future<JsonObject> setupBackend(JsonObject config) {
        Promise<JsonObject> promise = Promise.promise();
        AtomicReference<StorageBackend> storageBackend = new AtomicReference<>();

        setupStorageBackend(config).compose(sb -> {
                    storageBackend.set(sb);
                    return setupMongoDBConnection(config);
                }).onFailure(promise::fail)
                .onSuccess(dbHandler -> {
                    String storageLocation = config.getString(Constants.ENV_STORAGE_BACKEND, "").equals("S3") ?
                            config.getString(Constants.ENV_STORAGE_DEFAULT_BUCKET, Constants.DEFAULT_STORAGE_DEFAULT_LOCATION) :
                            config.getString(Constants.ENV_STORAGE_DEFAULT_DIRECTORY, Constants.DEFAULT_STORAGE_DEFAULT_LOCATION);

                    dataManager = new DataManager(dbHandler,
                            storageBackend.get(),
                            storageLocation);
                    promise.complete(config);
                });
        return promise.future();
    }

    private Future<StorageBackend> setupStorageBackend(JsonObject config) {

        Promise<StorageBackend> promise = Promise.promise();

        try {
            StorageBackend storageBackend;
            if (config.getString(Constants.ENV_STORAGE_BACKEND, "").equals("S3")) {
                String endpoint = config.getString(Constants.ENV_S3_ENDPOINT);
                String accessKey = config.getString(Constants.ENV_S3_ACCESSKEY);
                String secret = config.getString(Constants.ENV_S3_SECRET);
                String region = config.getString(Constants.ENV_S3_REGION);
                String directory = config.getString(Constants.ENV_STORAGE_DEFAULT_BUCKET);

                storageBackend = new MinioStorageBackendImpl(vertx, endpoint, accessKey, secret, region, directory);

                log.info("Successfully configured minio storage backend.");
            } else {
                storageBackend = new FilesystemStorageBackendImpl(vertx);
                log.info("Successfully configured File storage backend.");
            }
            promise.complete(storageBackend);
        } catch (Throwable t) {
            promise.fail(t);
        }

        return promise.future();
    }


    private Future<DBHandler> setupMongoDBConnection(JsonObject config) {
        Promise<DBHandler> promise = Promise.promise();
        JsonObject dbConfig = new JsonObject()
                .put("connection_string", config.getString(Constants.ENV_MONGO_DB_URI, "mongodb://localhost:27017"))
                .put("db_name", config.getString(Constants.ENV_MONGO_DB_NAME, "piveau"));
//                .put("connectTimeoutMS", 30)
//                .put("socketTimeoutMS", 30);
//                .put("keepAlive", true);

        log.debug(dbConfig.encodePrettily());


        MongoClient mongoClient = MongoClient.createShared(vertx, dbConfig);

        mongoClient.runCommand("ping", new JsonObject().put("ping", 1))
                .onFailure(err -> {
                    log.error("Failed connecting to mongodb:" + err.getMessage());
                    mongoClient.close();
                    vertx.close();
                    promise.fail(err);
                })
                .onSuccess(list -> {
                    DBHandler dbHandler = new DBHandler(
                            mongoClient,
                            config.getString(Constants.ENV_MONGO_DB_COLLECTION, "data")
                    );


                    if (ObjectUtils.allNotNull(mongoClient)) {
                        log.debug("Successfully started mongodb connection.");
                        promise.complete(dbHandler);

                    } else {
                        log.error("Failed starting mongodb connection:" + promise.future().result());
                        promise.fail("No DB-Connection.");
                    }
                });


        return promise.future();
    }

    private Future<Void> startServer(JsonObject config) {
        Promise<Void> promise = Promise.promise();
        JsonObject authConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_STORE_AUTH_CONFIG);
        RouterBuilder.create(vertx, "webroot/openapi.yaml")
                .onSuccess(builder -> {
                    PiveauAuth.create(vertx, new PiveauAuthConfig(authConfig))
                            .compose(piveauAuth -> {
                                WebClient client = WebClient.create(vertx);

                                Set<String> allowedHeaders = Set.of(
                                        "x-requested-with",
                                        "Content-Type",
                                        "Accept",
                                        "Authorization");

                                Set<HttpMethod> allowedMethods = Set.of(
                                        HttpMethod.GET,
                                        HttpMethod.OPTIONS,
                                        HttpMethod.DELETE,
                                        HttpMethod.PUT,
                                        HttpMethod.POST
                                );

//                                String corsDomains = "^(https?:\\/\\/(?:.+\\.)?(?:fokus\\.fraunhofer\\.de|localhost|data\\.europa\\.eu)(?::\\d{1,5})?)$";
                                List<String> corsDomains = config.getJsonArray(Constants.ENV_PIVEAU_STORE_CORS_DOMAINS, new JsonArray().add("*")).stream().map(Object::toString).toList();
                                builder.rootHandler(CorsHandler.create().addOrigins(corsDomains).allowedHeaders(allowedHeaders).allowedMethods(allowedMethods).allowCredentials(true));

                                builder.rootHandler(BodyHandler.create());
                                builder.rootHandler(StaticHandler.create());

                                builder.securityHandler("BearerAuth", piveauAuth.authHandler());

                                builder.operation("getData").handler(this::handleGetData);

                                builder.operation("putData")
                                        .handler(new PermissionHandler(List.of(
                                                Constants.AUTH_SCOPE_DATASET_CREATE,
                                                Constants.AUTH_SCOPE_DATASET_UPDATE)))
                                        .handler(this::handlePutData);

                                builder.operation("postData")
                                        .handler(new PermissionHandler(List.of(
                                                Constants.AUTH_SCOPE_DATASET_CREATE,
                                                Constants.AUTH_SCOPE_DATASET_UPDATE)))
                                        .handler(this::handlePostData);

                                builder.operation("deleteData")
                                        .handler(new PermissionHandler(List.of(
                                                Constants.AUTH_SCOPE_DATASET_DELETE)))
                                        .handler(this::handleDeleteData);

                                Router router = builder.createRouter();

                                router.route("/images/logo").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_LOGO_PATH, "webroot/images/logo.png"), client));
                                router.route("/images/favicon").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_FAVICON_PATH, "webroot/images/favicon.png"), client));
                                router.route("/imprint").handler(context ->
                                        context.redirect(config.getString(Constants.ENV_PIVEAU_IMPRINT_URL, "/")));

                                router.route("/privacy").handler(context ->
                                        context.redirect(config.getString(Constants.ENV_PIVEAU_PRIVACY_URL, "/")));

                                HealthCheckHandler hch = HealthCheckHandler.create(vertx);
                                hch.register("buildInfo", status -> status.complete(Status.OK(buildInfo)));
                                router.get("/health").handler(hch);

                                router.errorHandler(400, context -> {
                                    if (context.failure() instanceof BadRequestException failure) {
                                        JsonObject error = new JsonObject()
                                                .put("status", "error")
                                                .put("message", failure.getMessage());
                                        context.response().setStatusCode(400).putHeader("Content-Type", "application/json").end(error.encodePrettily());
                                    }
                                });

                                return vertx
                                        .createHttpServer(new HttpServerOptions().setPort(config.getInteger(Constants.ENV_HTTP_PORT, 8080)))
                                        .requestHandler(router)
                                        .listen();
                            })
                            .onSuccess(server -> {
                                log.info("Listening on Port " + server.actualPort());
                                promise.complete();
                            })
                            .onFailure(promise::fail);

                }).onFailure(promise::fail);

        return promise.future();
    }

    private void handlePostData(RoutingContext context) {
        if (context.fileUploads().size() != 1) {
            context.response().setStatusCode(400).end(failureJson("Only one file allowed in upload", 400));
            log.error("Number of files uploaded: {}", context.fileUploads().size());
            return;
        }
        FileUpload file = context.fileUploads().get(0);
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        dataManager.save(file.uploadedFileName(), file.fileName(), file.size(), parameters.queryParameter("catalog").getString())
                .onSuccess(id -> context.response()
                        .setStatusCode(201)
                        .putHeader("Location", "/data/" + id)
                        .end(successJson(false, file, id)))
                .onFailure(cause -> {
                    context.response().setStatusCode(500).end(failureJson(cause.getMessage(), 500));
                });
    }

    private void handlePutData(RoutingContext context) {
        log.debug("Enter put data");
        if (context.fileUploads().size() != 1) {
            context.response().setStatusCode(400).end(failureJson("Only one file allowed in upload", 400));
            log.error("Number of files uploaded: {}", context.fileUploads().size());
            return;
        }

        FileUpload file = context.fileUploads().get(0);
        log.debug("Upload of {}", file.fileName());

        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        dataManager.save(file.uploadedFileName(), file.fileName(), file.size(), parameters.pathParameter("id").getString(), parameters.queryParameter("catalog").getString())
                .onSuccess(code -> context.response()
                        .setStatusCode(code)
                        .putHeader("Location", "/data/" + parameters.pathParameter("id").getString())
                        .end(successJson(code != 201, file, parameters.pathParameter("id").getString())))
                .onFailure(cause -> {
                    context.response().setStatusCode(500).end(failureJson(cause.getMessage(), 500));
                });
    }


    private String successJson(boolean updated, FileUpload fileUpload, String id) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.put("success", true);
        JsonObject result = new JsonObject();
        result.put("filename", fileUpload.fileName());
        result.put("id", id);
        result.put("location", "/data/" + id);
        result.put("size", fileUpload.size());
        result.put("mimetype", fileUpload.contentType());
        result.put("updated", updated);
        jsonObject.put("result", result);

        return jsonObject.encodePrettily();
    }


    private String successJson(String id) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.put("success", true);
        JsonObject result = new JsonObject();
        result.put("id", id);
        jsonObject.put("result", result);

        return jsonObject.encodePrettily();
    }

    private String failureJson(String error, int code) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.put("success", false);

        if (!error.isEmpty()) {
            jsonObject.put("code", code);
            jsonObject.put("error", error);
        }

        return jsonObject.encodePrettily();
    }


    private void handleDeleteData(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        dataManager.remove(parameters.pathParameter("id").getString())
                .onSuccess(a -> context.response().setStatusCode(HttpResponseStatus.ACCEPTED.code()).end(successJson(parameters.pathParameter("id").getString())))
                .onFailure(cause -> {
                    if (cause instanceof ServiceException se) {
                        context.response().setStatusCode(se.failureCode()).end(failureJson(se.getMessage(), se.failureCode()));
                    } else {
                        if (cause.getMessage() != null) {
                            int code = cause.getMessage().equals(HttpResponseStatus.NOT_FOUND.reasonPhrase()) ? 404 : 500;
                            context.response().setStatusCode(code).end(failureJson(cause.getLocalizedMessage(), code));
                        } else {
                            context.fail(500);
                        }
                    }
                });
    }

    private void handleGetData(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String id = parameters.pathParameter("id").getString();
        dataManager.getName(id).compose(filedescriptor -> {
                    if (filedescriptor.getSize() > 0) {
                        context.response().putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(filedescriptor.getSize()));
                    }
                    context.response()
                            .putHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filedescriptor.getFilename() + "\"")
                            .setStatusCode(200).setChunked(true);

                    return dataManager.getFile(id, context.response());
                })
                .onSuccess(v -> context.response())
                .onFailure(cause -> {
                    log.error(cause.getMessage());
                    if (!context.response().headWritten()) {
                        context.response().putHeader(HttpHeaders.CONTENT_DISPOSITION, "");
                        context.response().setChunked(false);
                    }
                    if (cause instanceof ServiceException se) {
                        context.response().setStatusCode(se.failureCode());
                        context.response().end(failureJson(se.getMessage(), se.failureCode()));
                    } else {
                        if (cause.getMessage() != null) {
                            int code = cause.getMessage().equals(HttpResponseStatus.NOT_FOUND.reasonPhrase()) ? 404 : 500;
                            context.response().setStatusCode(code).end(failureJson(cause.getLocalizedMessage(), code));
                        } else {
                            context.fail(500);
                        }
                    }
                });
    }
}
