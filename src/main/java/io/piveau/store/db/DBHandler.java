package io.piveau.store.db;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.piveau.store.Constants;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;
import io.vertx.ext.mongo.WriteOption;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

public class DBHandler {

    private static final Logger log = LoggerFactory.getLogger(DBHandler.class);

    private final MongoClient mongoClient;
    private final String dbName;

    public DBHandler(MongoClient mongoClient, String dbName) {
        this.mongoClient = mongoClient;
        this.dbName = dbName;
    }

    public Future<Integer> upsertDocument(String fileName, String id, Long fileSize, String catalog) {
        JsonObject query = new JsonObject().put("_id", id);
        JsonObject document = new JsonObject()
                .put("_id", id)
                .put("fileName", fileName)
                .put("fileSize", fileSize)
                .put("fileLastModified", Instant.now().getEpochSecond())
                .put("catalog", catalog);

        return mongoClient.replaceDocumentsWithOptions(dbName, query, document, new UpdateOptions().setUpsert(true))
                .map(result -> result.getDocUpsertedId() != null ? 201 : 200);
    }

    public Future<String> insertDocument(String fileName, Long fileSize, String catalog) {
        JsonObject document = new JsonObject()
                .put("fileName", fileName)
                .put("fileSize", fileSize)
                .put("fileLastModified", Instant.now().getEpochSecond())
                .put("catalog", catalog);

        return mongoClient.save(dbName, document);
    }

    public Future<Integer> removeDocument(String id, String catalog) {
        JsonObject document = new JsonObject().put("_id", id).put("catalog" , catalog);
        return mongoClient.removeDocumentWithOptions(dbName, document, WriteOption.ACKNOWLEDGED)
                .map(result -> result.getRemovedCount() == 1 ? 204 : 404);
    }

    public Future<JsonObject> findDocument(String id) {
        JsonObject queryDocument = new JsonObject()
                .put("_id", id);
             //   .put("catalog", "");

        return mongoClient.findOne(dbName, queryDocument, null)
                .compose(result -> {
                    if (result == null) {
                        return Future.failedFuture(new ServiceException(404, HttpResponseStatus.NOT_FOUND.reasonPhrase()));
                    } else {
                        return Future.succeededFuture(result);
                    }
                });
    }

    public Future<JsonObject> findDocumentForRemove(String id,String catalog) {
        JsonObject queryDocument = new JsonObject()
                .put("_id", id).put("catalog", catalog);

        return mongoClient.findOne(dbName, queryDocument, null)
                .compose(result -> {
                    if (result == null) {
                        return Future.failedFuture(new ServiceException(404, HttpResponseStatus.NOT_FOUND.reasonPhrase()));
                    } else {
                        return Future.succeededFuture(result);
                    }
                });
    }

    public Future<Void> cleanup() {
        return mongoClient.dropCollection(dbName);
    }

    public static Future<DBHandler> create(Vertx vertx, JsonObject config) {
        JsonObject dbConfig = new JsonObject()
                .put("connection_string", config.getString(Constants.ENV_MONGO_DB_URI, "mongodb://localhost:27017"))
                .put("db_name", config.getString(Constants.ENV_MONGO_DB_NAME, "piveau"));
//                .put("connectTimeoutMS", 30)
//                .put("socketTimeoutMS", 30);
//                .put("keepAlive", true);

        // TODO Allow username and password

        if (log.isDebugEnabled()) {
            log.debug("Metadata database configuration: {}", dbConfig.encodePrettily());
        }

        MongoClient mongoClient = MongoClient.createShared(vertx, dbConfig);
        DBHandler dbHandler = new DBHandler(
                mongoClient,
                config.getString(Constants.ENV_MONGO_DB_COLLECTION, "data")
        );
        return mongoClient.runCommand("ping", new JsonObject().put("ping", 1))
                .onSuccess(list -> log.debug("Successfully started mongodb connection."))
                .onFailure(cause -> {
                    log.error("Ping mongo db", cause);
                    mongoClient.close();
                })
                .map(dbHandler);
    }
}
