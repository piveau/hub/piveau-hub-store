package io.piveau.store.security;

import io.vertx.ext.web.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.capybara.clamav.ClamavClient;
import xyz.capybara.clamav.ClamavException;
import xyz.capybara.clamav.CommunicationException;
import xyz.capybara.clamav.commands.scan.result.ScanResult;

import java.io.*;

public class VirusScanner {

    private static final Logger log = LoggerFactory.getLogger(VirusScanner.class);

    private final ClamavClient client;

    VirusScanner(String host, int port) {
        client = new ClamavClient(host, port);
    }

    public static VirusScanner create(String daemonHost, Integer daemonPort) {
        VirusScanner scanner = new VirusScanner(daemonHost,daemonPort);
        try {
            scanner.client.ping();
            return scanner;
        } catch (ClamavException e) {
            Throwable embedded = e.getCause();
            if (embedded instanceof CommunicationException) {
                log.error("Ping clamav daemon", embedded);
            } else {
                log.error("Ping clamav daemon", embedded.getCause());
            }
            return null;
        }
    }

    public ScanResult scanFile(FileUpload fileUpload) throws IOException {
        try (InputStream inputStream = new FileInputStream(fileUpload.uploadedFileName())) {
            return client.scan(inputStream);
        }
    }

}
