package io.piveau.store;

import io.piveau.security.PiveauAuth;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class PermissionHandler implements Handler<RoutingContext>  {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final List<String> scopes;

    public PermissionHandler(List<String> scopes) {
        this.scopes = scopes;
    }

    @Override
    public void handle(RoutingContext context) {
        String resource = context.queryParams().get("catalog");
        if (log.isTraceEnabled()) {
            log.trace("Permission check for {}: {}", resource, context.user().principal().encodePrettily());
        }

        if (PiveauAuth.userHasRole(context.user(), "operator")
                || scopes.stream().allMatch(scope -> PiveauAuth.userHasPermission(context.user(), resource, scope))) {
            context.next();
        } else {
            context.fail(403);
        }
    }
}


