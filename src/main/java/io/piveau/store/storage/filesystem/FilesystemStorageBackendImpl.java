package io.piveau.store.storage.filesystem;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.piveau.store.Constants;
import io.piveau.store.storage.StorageBackend;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.CopyOptions;
import io.vertx.core.file.FileSystem;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.WriteStream;
import io.vertx.serviceproxy.ServiceException;

import java.io.File;
import java.nio.file.NoSuchFileException;
import java.util.Map;

public class FilesystemStorageBackendImpl extends StorageBackend {

    private final FileSystem fs;

    private final String location;

    public FilesystemStorageBackendImpl(Vertx vertx, JsonObject config, Promise<StorageBackend> promise) {
        fs = vertx.fileSystem();
        location = config.getString(Constants.ENV_STORAGE_DEFAULT_LOCATION, Constants.STORAGE_DEFAULT_LOCATION);

        createDirectoryIfNotExists(location).map((StorageBackend) this).onComplete(promise);
    }

    @Override
    public String type() {
        return "FILE";
    }

    @Override
    public Future<Void> createDirectory(String directory) {
        return fs.mkdir(directory);
    }

    @Override
    public Future<Void> createDirectoryIfNotExists(String directory) {
        return directoryExists(directory)
                .compose(exist -> {
                    if (Boolean.FALSE.equals(exist)) {
                        return createDirectory(directory);
                    } else {
                        return Future.succeededFuture();
                    }
                });
    }

    @Override
    public Future<Void> removeDirectory(String directory) {
        return fs.delete(directory);
    }

    @Override
    public Future<Boolean> directoryExists(String directory) {
        return fs.exists(directory);
    }

    @Override
    public Future<Void> putFile(String name, String uploadName, Map<String, String> metadata) {
        String path = new File(location, name).getPath();
        return fs.move(uploadName, path, new CopyOptions().setReplaceExisting(true));
    }

    @Override
    public Future<Void> getFile(String name, WriteStream<Buffer> writeStream) {
        String path = new File(location, name).getPath();
        return Future.future(promise ->
                fs.open(path, new OpenOptions().setCreate(false).setRead(true).setWrite(false))
                        .onSuccess(file -> {
                            file.pipeTo(writeStream);
                            promise.complete();
                        })
                        .onFailure(cause -> {
                            if (cause instanceof NoSuchFileException) {
                                promise.fail(new ServiceException(404, HttpResponseStatus.NOT_FOUND.reasonPhrase()));
                            } else {
                                promise.fail(cause);
                            }
                        }));
    }

    @Override
    public Future<Void> removeFile(String name) {
        String path = new File(location, name).getPath();
        return Future.future(promise ->
            fs.delete(path)
                    .onSuccess(v -> promise.complete())
                    .onFailure(cause -> {
                        if (cause instanceof NoSuchFileException) {
                            promise.fail(new ServiceException(404, HttpResponseStatus.NOT_FOUND.reasonPhrase()));
                        } else {
                            promise.fail(cause);
                        }
                    }));
    }

}
