package io.piveau.store.storage.minio;

import io.minio.*;
import io.minio.credentials.IamAwsProvider;
import io.minio.MinioClient.Builder;
import io.piveau.store.AsyncInputStream;
import io.piveau.store.storage.StorageBackend;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.WorkerExecutor;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.streams.WriteStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class MinioStorageBackendImpl extends StorageBackend {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final MinioClient minioClient;
    private final Vertx vertx;
    private final WorkerExecutor executor;

    private final String bucket;

    public MinioStorageBackendImpl(Vertx vertx, String endpoint, String accessKey, String secret, String region, String bucket, Promise<StorageBackend> promise) {
        this.vertx = vertx;
        this.bucket = bucket;

        // TODO This WorkerExecutor is just a workaround. Refactoring to a real WorkerVerticle is advised.
        executor = vertx.createSharedWorkerExecutor("minio-worker-pool", 20, 10, TimeUnit.MINUTES);

        Builder minioClientBuilder = MinioClient.builder();
        minioClientBuilder.endpoint(endpoint);

        if (region != null) {
            minioClientBuilder.region(region);
        }

        if (secret != null && accessKey != null) {
            minioClientBuilder.credentials(accessKey, secret);
        } else {
            minioClientBuilder.credentialsProvider(new IamAwsProvider(null, null));
        }

        minioClient = minioClientBuilder.build();
        executor.<StorageBackend>executeBlocking(p -> {
            try {
                if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucket).build())) {
                    minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
                };
                log.debug("Successfully connected to S3");
                p.complete(this);
            } catch (Exception e) {
                log.error("Connecting to S3", e);
                p.fail(e);
            }
        }).onComplete(promise);
   }

    @Override
    public String type() {
        return "S3";
    }

    public Future<Void> createDirectory(String directory) {
        return createDirectoryIfNotExists(directory);
    }

    public Future<Void> removeDirectory(String directory) {
        return directoryExists(directory)
                .compose(exist -> {
                    if (Boolean.TRUE.equals(exist)) {
                        return executor.executeBlocking(promise -> {
                            try {
                                minioClient.removeBucket(RemoveBucketArgs.builder().bucket(directory).build());
                                promise.complete();
                            } catch (Exception e) {
                                promise.fail(e);
                            }
                        });
                    } else {
                        return Future.failedFuture("Bucket does not exist");
                    }
                });
    }


    public Future<Void> createDirectoryIfNotExists(String directory) {
        return directoryExists(directory)
                .compose(exist -> {
                    if (Boolean.TRUE.equals(exist)) {
                        return Future.succeededFuture();
                    } else {
                        return executor.executeBlocking(promise -> {
                            try {
                                minioClient.makeBucket(MakeBucketArgs.builder().bucket(directory).build());
                                promise.complete();
                            } catch (Exception e) {
                                promise.fail(e);
                            }
                        });
                    }
                });
    }

    public Future<Boolean> directoryExists(String directory) {
        return executor.executeBlocking(promise -> {
            try {
                Boolean exists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(directory).build());
                promise.complete(exists);
            } catch (Exception e) {
                promise.fail(e);
            }
        });
    }

    @Override
    public Future<Void> putFile(String name, String uploadName, Map<String, String> metadata) {
        log.debug("bucket = {}, name = {}, upload name = {}", bucket, name, uploadName);
        return executor.executeBlocking(promise -> {
            try {
                minioClient.uploadObject(
                        UploadObjectArgs.builder()
                                .bucket(bucket)
                                .object(name)
                                .filename(uploadName)
                                .userMetadata(metadata)
//                                .contentType("plain/text")
                                .build());

                vertx.fileSystem().deleteBlocking(uploadName);
                promise.complete();
            } catch (Exception e) {
                vertx.fileSystem().exists(uploadName).onSuccess(exists -> {
                    if (Boolean.TRUE.equals(exists)) vertx.fileSystem().deleteBlocking(uploadName);
                });
                promise.fail(e);
            }
        });
    }

    public Future<Void> getFile(String name, WriteStream<Buffer> writeStream) {
        return executor.executeBlocking(promise -> {
            try {
                InputStream stream = minioClient.getObject(GetObjectArgs.builder().bucket(bucket).object(name).build());
                AsyncInputStream asyncInputStream = new AsyncInputStream(vertx, vertx.getOrCreateContext(), stream);
                asyncInputStream.pipeTo(writeStream);
                promise.complete();
            } catch (Exception e) {
                log.error("Get minio file", e);
                promise.fail(e);
            }
        });
    }

    public Future<Void> removeFile(String name) {
        return executor.executeBlocking(promise -> {
            try {
                minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucket).object(name).build());
                promise.complete();
            } catch (Exception t) {
                promise.fail(t);
            }
        });
    }

}
