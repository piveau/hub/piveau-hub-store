package io.piveau.store.storage;

import io.piveau.store.Constants;
import io.piveau.store.storage.filesystem.FilesystemStorageBackendImpl;
import io.piveau.store.storage.minio.MinioStorageBackendImpl;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.WriteStream;

import java.util.Map;

public abstract class StorageBackend {

    public abstract Future<Void> createDirectory(String directory);

    public abstract Future<Void> createDirectoryIfNotExists(String directory);

    public abstract Future<Void> removeDirectory(String directory);

    public abstract Future<Boolean> directoryExists(String directory);

    public abstract Future<Void> putFile(String name, String uploadedName, Map<String, String> metadata);

    public abstract Future<Void> getFile(String name, WriteStream<Buffer> writeStream);

    public abstract Future<Void> removeFile(String name);

    public abstract String type();

    public static Future<StorageBackend> create(Vertx vertx, JsonObject config) {
        return Future.future(promise -> {
            String storageBackend = config.getString(Constants.ENV_STORAGE_BACKEND, Constants.DEFAULT_STORAGE_BACKEND);
            switch (storageBackend) {
                case "S3" -> {
                    String endpoint = config.getString(Constants.ENV_S3_ENDPOINT);
                    String accessKey = config.getString(Constants.ENV_S3_ACCESSKEY);
                    String secret = config.getString(Constants.ENV_S3_SECRET);
                    String region = config.getString(Constants.ENV_S3_REGION);
                    String bucket = config.getString(Constants.ENV_STORAGE_DEFAULT_BUCKET, "piveau-data-store");
                    new MinioStorageBackendImpl(vertx, endpoint, accessKey, secret, region, bucket, promise);
                }
                case "FILE" -> new FilesystemStorageBackendImpl(vertx, config, promise);
                default -> promise.fail("Unknown storage backend " + storageBackend);
            }
        });
    }

}
