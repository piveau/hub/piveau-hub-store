package io.piveau.store;

public class Constants {

    private Constants() {}

    // MongoDB

    public static final String ENV_MONGO_DB_URI = "MONGO_DB_URI";
    public static final String ENV_MONGO_DB_NAME = "MONGO_DB_NAME";
    public static final String ENV_MONGO_DB_COLLECTION = "MONGO_DB_COLLECTION";
    public static final String ENV_MONGO_DB_CONFIGURATION = "MONGO_DB_CONFIGURATION";

    // Security

    public static final String ENV_PIVEAU_STORE_AUTH_CONFIG = "PIVEAU_STORE_AUTH_CONFIG";
    public static final String ENV_PIVEAU_STORE_CORS_DOMAINS = "PIVEAU_STORE_CORS_DOMAINS";

    // Storage Backend

    public static final String ENV_STORAGE_BACKEND = "STORAGE_BACKEND";
    public static final String DEFAULT_STORAGE_BACKEND = "FILE";
    public static final String ENV_STORAGE_DEFAULT_BUCKET = "STORAGE_DEFAULT_BUCKET";
    public static final String ENV_STORAGE_DEFAULT_LOCATION = "STORAGE_DEFAULT_DIRECTORY";
    public static final String STORAGE_DEFAULT_LOCATION = "data-store";
    public static final String STORAGE_DEFAULT_LOCATION_DEPRECATED = "simplestore";

    // S3 Configurations

    public static final String ENV_S3_ENDPOINT = "S3_ENDPOINT";
    public static final String ENV_S3_ACCESSKEY = "S3_ACCESSKEY";
    public static final String ENV_S3_SECRET = "S3_SECRET";
    public static final String ENV_S3_REGION = "S3_REGION";

    // Misc

    public static final String ENV_HTTP_PORT = "HTTP_PORT";

    public static final String ENV_PIVEAU_IMPRINT_URL = "PIVEAU_IMPRINT_URL";
    public static final String ENV_PIVEAU_PRIVACY_URL = "PIVEAU_PRIVACY_URL";
    public static final String ENV_PIVEAU_LOGO_PATH = "PIVEAU_LOGO_PATH";
    public static final String ENV_PIVEAU_FAVICON_PATH = "PIVEAU_FAVICON_PATH";

    // Keycloak Scopes

    public static final String AUTH_SCOPE_DATASET_CREATE = "dataset:create";
    public static final String AUTH_SCOPE_DATASET_UPDATE = "dataset:update";
    public static final String AUTH_SCOPE_DATASET_DELETE = "dataset:delete";

    public static final String ENV_ALLOWED_MIME_TYPES = "ALLOWED_MIME_TYPES";

    // Clam AV
    public static final String ENV_CLAMAV_HOST = "CLAMAV_HOST";
    public static final String ENV_CLAMAV_PORT = "CLAMAV_PORT";
    public static final int DEFAULT_CLAMAV_PORT = 3310;
    public static final String DEFAULT_CLAMAV_HOST = "localhost";
    public static final String ENV_CLAMAV_ENABLED = "CLAMAV_ENABLED";
    public static final boolean DEFAULT_CLAMAV_ENABLED = false;


}
