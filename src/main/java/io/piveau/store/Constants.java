package io.piveau.store;

public class Constants {

    // Configuration variables
    static public final String ENV_MONGO_DB_URI = "MONGO_DB_URI";
    static public final String ENV_MONGO_DB_NAME = "MONGO_DB_NAME";
    static public final String ENV_MONGO_DB_COLLECTION = "MONGO_DB_COLLECTION";
    static public final String ENV_HTTP_PORT = "HTTP_PORT";
    static public final String ENV_PIVEAU_STORE_AUTH_CONFIG = "PIVEAU_STORE_AUTH_CONFIG";

    static public final String ENV_PIVEAU_STORE_CORS_DOMAINS = "PIVEAU_STORE_CORS_DOMAINS";

    static public final String ENV_STORAGE_BACKEND = "STORAGE_BACKEND";
    static public final String ENV_STORAGE_DEFAULT_DIRECTORY = "STORAGE_DEFAULT_DIRECTORY";
    static public final String ENV_STORAGE_DEFAULT_BUCKET = "STORAGE_DEFAULT_BUCKET";
    static public final String DEFAULT_STORAGE_DEFAULT_LOCATION = "simplestore";

    static public final String ENV_S3_ENDPOINT = "S3_ENDPOINT";
    static public final String ENV_S3_ACCESSKEY = "S3_ACCESSKEY";
    static public final String ENV_S3_SECRET = "S3_SECRET";
    static public final String ENV_S3_REGION = "S3_REGION";

    public static final String ENV_PIVEAU_IMPRINT_URL = "PIVEAU_IMPRINT_URL";
    public static final String ENV_PIVEAU_PRIVACY_URL = "PIVEAU_PRIVACY_URL";
    public static final String ENV_PIVEAU_LOGO_PATH = "PIVEAU_LOGO_PATH";
    public static final String ENV_PIVEAU_FAVICON_PATH = "PIVEAU_FAVICON_PATH";

    // Keycloak Scopes
    public static final String AUTH_SCOPE_DATASET_CREATE = "dataset:create";
    public static final String AUTH_SCOPE_DATASET_UPDATE = "dataset:update";
    public static final String AUTH_SCOPE_DATASET_DELETE = "dataset:delete";

}
