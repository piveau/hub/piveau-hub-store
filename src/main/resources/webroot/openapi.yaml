openapi: 3.0.3
info:
  version: '${project.version}'
  title: hub-store
  description: A service to easily upload and store data either on the filesystem or in an S3 service
  x-logo:
    url: images/logo


servers:
  - url: ''
    description: Local instance for development
tags:
  - name: Data
    x-displayName: Data
    description: Store and manage data.

paths:
  /data:
    post:
      summary: Post data
      description: Upload a file without providing an ID
      operationId: postData
      tags:
        - Data
      security:
        - BearerAuth: []
      parameters:
        - name: catalog
          in: query
          description: "ID of the Catalogue"
          required: true
          schema:
            type: string
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                data:
                  type: string
        description: "Upload a file with the allowed mimetype"
      responses:
        '201':
          description: Created
          headers:
            Location:
              schema:
                type: string
              description: the location of the created resource
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
        '400':
          description: Bad Request
          headers: {}
        '401':
          description: Not Authorized
        '403':
          description: Forbidden
  '/data/{id}':
    get:
      parameters:
        - name: id
          in: path
          description: the data id
          required: true
          schema:
            type: string
            minLength: 1
      summary: Get data (download)
      description: Download data with a given id.
      operationId: getData
      tags:
        - Data
      responses:
        '200':
          description: Ok
        '404':
          description: Not Found
    put:
      summary: Put data (upload)
      description: Upload data with a given id. Replace it if it already exists.
      operationId: putData
      parameters:
        - name: id
          in: path
          description: the data id
          required: true
          schema:
            type: string
            minLength: 1
        - name: catalog
          in: query
          description: "ID of the Catalogue"
          required: true
          schema:
            type: string
      tags:
        - Data
      security:
        - BearerAuth: []
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                data:
                  type: string
      responses:
        '201':
          description: Data created
          headers:
            Location:
              description: the location of the created resource
              schema:
                type: string
                format: uri
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
        '204':
          description: Data updated
        '400':
          description: Bad request
        '401':
          description: Not authorized
        '403':
          description: Forbidden
    delete:
      summary: Delete data
      description: Delete data with a given id.
      operationId: deleteData
      parameters:
        - name: id
          in: path
          description: the data id
          required: true
          schema:
            type: string
            minLength: 1
        - name: catalog
          in: query
          description: "ID of the Catalogue"
          required: true
          schema:
            type: string
      tags:
        - Data
      security:
        - BearerAuth: []
      responses:
        '204':
          description: Data successfully deleted.
        '401':
          description: Not authorized.
        '403':
          description: Forbidden
        '404':
          description: Data not found.
components:
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: X-API-Key
    BearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    Response:
      description: ''
      type: object
      x-examples:
        example-1:
          success: true
          result:
            filename: 100K.txt
            id: 614b2d639155ce0a50b7d284
            location: /data/614b2d639155ce0a50b7d284
            size: 100000
            mimetype: text/plain
      title: ''
      properties:
        success:
          type: boolean
        result:
          type: object
          required:
            - filename
            - id
            - location
            - size
            - mimetype
          properties:
            filename:
              type: string
              minLength: 1
            id:
              type: string
              minLength: 1
            location:
              type: string
              minLength: 1
            size:
              type: number
              exclusiveMinimum: true
              minimum: 0
            mimetype:
              type: string
              minLength: 1
      required:
        - success
        - result
