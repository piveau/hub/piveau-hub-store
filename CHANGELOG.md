# ChangeLog piveau-hub-store

## Unreleased
## 3.1.2 (2024-10-29)

**Fixed:**
* Authorization security gap

## 3.1.1 (2024-10-29)

**Changed:**
* Set body size to unlimited for allow uploading files greater then 10MB

## 3.1.0 (2024-08-15)

**Added:**
* Clam AV integration to scan each uploaded files if it is a virus

**Changed:**
* helm chart now supports deploying a clam av daemon that is used in the clam av integration. It is disabled by default

## 3.0.1 (2024-08-11)

## 3.0.0 (2024-08-09)

**Added:**
* can now use the environment variable `ALLOWED_MIME_TYPES` to define a set of allowed file types.


**Changed:**
* Uses path `./data-store` for storing files (uses `./simplestore` if exists for backward compatibility)
  (Therefore, mount path for docker image is `/usr/verticles/data-store` or `/usr/verticles/simplestore`)
* Lib dependency updates
* Use test mongodb container
* Remove unused dependencies

## 2.0.1 (2023-03-23)

**Changed:**
* Return `403 Forbidden` when access not permitted

**Added:**
* More log outputs

## 2.0.0 (2023-03-23)

**Changed:**
* `KEYCLOAK_CONFIG` renamed to `PIVEAU_STORE_AUTH_CONFIG`

**Added:**
* `POST` to allowed CORS methods 

**Fixed:**
* Tests using root directory, which does not work on windows

## 1.0.1 (2023-03-22)

**Added:**
* Configuration of CORS domains via `PIVEAU_STORE_CORS_DOMAINS`

## 1.0.0 (2023-02-01)

**Added:**
* Health checks
* Helm chart

## 0.9.2 (2022-02-23)

**Added:**
* Access control based on catalog query parameter
* Fixed OpenAPI base URL
* Added ErrorHandler to router

## 0.9.1 (2022-02-11)

**Fixed:**
* Complete crash, when canceling a download


## 0.9.0 (2022-02-10)

* Initial release 
