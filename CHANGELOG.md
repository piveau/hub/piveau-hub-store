# ChangeLog piveau-hub-store

## Unreleased

## 2.0.1 (2023-03-23)

**Changed:**
* Return `403 Forbidden` when access not permitted

**Added:**
* More log outputs

## 2.0.0 (2023-03-23)

**Changed:**
* `KEYCLOAK_CONFIG` renamed to `PIVEAU_STORE_AUTH_CONFIG`

**Added:**
* `POST` to allowed CORS methods 

**Fixed:**
* Tests using root directory, which does not work on windows

## 1.0.1 (2023-03-22)

**Added:**
* Configuration of CORS domains via `PIVEAU_STORE_CORS_DOMAINS`

## 1.0.0 (2023-02-01)

**Added:**
* Health checks
* Helm chart

## 0.9.2 (2022-02-23)

**Added:**
* Access control based on catalog query parameter
* Fixed OpenAPI base URL
* Added ErrorHandler to router

## 0.9.1 (2022-02-11)

**Fixed:**
* Complete crash, when canceling a download


## 0.9.0 (2022-02-10)

* Initial release 
