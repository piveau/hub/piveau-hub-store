# piveau-hub-store

The piveau store provides a simple API for storing, retrieving and deleting files. The service can store these files
either on te server in a configurable folder or with an S3 compatible storage provider, like minio.

## Table of Contents

***

1. [Prerequisites](#prerequisites)
1. [Installation](#installation)
1. [Configuration](#configuration)
1. [Deployment](#deployment)
1. [API](#api)
1. [Key Decisions](#key-decisions)
1. [Code Quality](#quality)
1. [Known Issues](#known-issues)
1. [Maintainer](#maintainer)
1. [License](#license)

## Prerequisites

***
Install the following software

* MongoDB >= 5
* Java version >= 15
* Git >= 2.17
* Optional: A [MinIO server](https://min.io/) or AWS S3 bucket, if this is used as storage backend

## Installation

***

Clone the directory and enter it

```bash
   $ git clone git@gitlab.fokus.fraunhofer.de:piveau/hub/piveau-hub-store.git
   $ cd piveau-hub-store
```

Start a MongoDB. You can use the included Docker Compose:

```bash
   $ docker-compose up -d
```

Build the project by using Maven and run the application. The generated _jar_ can be found in the `target` directory.

```bash
   $ mvn clean package
   $ java -jar target/store.jar
```

As an alternative, you can also run the project in Docker:

1. Start your docker daemon
2. Build the application as described
3. Build the image: `docker build -t hub-store .`
3. Configure the environment variables as needed.
2. Make sure that MongDB is running
5. Run the image: `docker run -it -p 8080:8080 hub-store`

## Configuration

***
Edit the environment variables according to your local setup. Variables and their purpose are listed below:

| Key                       | Description                                                                             | Default                   | Type     |
|:--------------------------|:----------------------------------------------------------------------------------------|:--------------------------|:---------|
| MONGO_DB_URI              | The URL for the mongodb used by this service                                            | mongodb://localhost:27017 | `string` |
| MONGO_DB_NAME             | The name for the database the files will be stored in                                   | piveau                    | `string` |
| MONGO_DB_COLLECTION       | The name for the database the files will be stored in                                   | data                      | `string` |
| MONGO_USER                | The User for the database                                                               |                           | `string` |
| MONGO_PW                  | The password for the database                                                           |                           | `string` |
| HTTP_PORT                 | Port used to communicate with the API                                                   | 8080                      | `int`    |
| PIVEAU_STORE_AUTH_CONFIG  | For fetching public keys and authentication                                             |                           | `object` |
| PIVEAU_STORE_CORS_DOMAINS | Setting allowed CORS domains                                                            | `["*"]`                   | `array`  |
| STORAGE_BACKEND           | `S3` or `FILE`, to specify the storage backend to be used                               | `FILE`                    | `enum`   | 
| STORAGE_DEFAULT_DIRECTORY | The base directory to save the files. Only used with `STORAGE_BACKEND` != `S3`          | `simplestore`             | `string` |
| STORAGE_DEFAULT_BUCKET    | The S3 bucket to save the files. Only used with `STORAGE_BACKEND` = `S3`                | `simplestore`             | `string` | 
| S3_ENDPOINT               | if `STORAGE_BACKEND` is `S3`, this will be used to set the S3 endpoint                  |                           | `string` |
| S3_ACCESSKEY              | if `STORAGE_BACKEND` is `S3`, this will be used to set the S3 access key/username       |                           | `string` |
| S3_SECRET                 | if `STORAGE_BACKEND` is `S3`, this will be used to set the S3 secret key/password       |                           | `string` |
| S3_REGION                 | if `STORAGE_BACKEND` is `S3`, this will be used to set the S3 region, e.g. eu-central-1 |                           | `string` |

## Deployment

***
Deployment should be done using [Docker](https://www.docker.com/) containers. Changes to the `develop` branch are
deployed automatically to the OSC namespaces piveau and data-europe-eu. Changes to the `master` branch are deployed
automatically to PPE, Tags are deployed to PROD. View the [`.gitlab-ci.yml`](.gitlab-ci.yml) file for details. Make sure
to follow the [recommendations](https://docs.mongodb.com/manual/administration/production-notes/) before using the
database in production.

## API

***
A formal OpenAPI 3 specification can be found in
the [/src/main/resources/webroot/openapi.yaml](/src/main/resources/webroot/openapi.yaml) file. A visually more appealing
version is available at `{url}:{port}` once the application has been started.

## Security
***
The service uses our Keycloak infrastructure for access control. For local development or deployment without Keycloak you can easily sign our own token. 
Set a custom secret in the Keycloak configuration instead of a real Keycloak:
```json
{
  "PIVEAU_STORE_AUTH_CONFIG": {
    "customSecret": "secret"
  }
}
```
Take the following JWT as a blueprint to sign it with the secret above (e.g. "secret") on [JWT.io](https://jwt.io). 
```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTk3NjgyNDAsImV4cCI6NDgwNjk3ODAwNiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbInByb3ZpZGVyIl19fQ.WnGJHZaLYSKwyfMHuErCCj4V7sWBoh2Me3vxrAbwYdM
```
Take the result as bearer token for the requests.

## Key Decisions

***

- If the user tries to upload a file to an ID that already exists, the old file will be overwritten without warning.
  There will also be no check, if the user is the owner of this ID.
- Deleting a file happens asynchronously to the server response.

## Quality

***
You can find the code Quality
results [here](https://sonarqube.apps.osc.fokus.fraunhofer.de/dashboard?id=io.piveau.hub%3Astore).

## Known issues

***

- Upload to a minio instance with an account that is restricted to a specific Bucket results in Access Denied Error.
- as the minio upload is blocking, large file uploads (>1GB) result in thread blocked warnings atm. this could be
  handled better.

## Maintainers

***
[Torben Jastrow](mailto:Torben.Jastrow@fokus.fraunhofer.de)
[Fabian Kirstein](mailto:fabian.kirstein@fokus.fraunhofer.de)
[Simon Dutkowski](mailto:simon.dutkowski@fokus.fraunhofer.de)

## License

***
[Apache License, Version 2.0](LICENSE.md)
